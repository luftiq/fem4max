classdef MaxwFEM
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Class for matrix assembly for maxwell's time-harmonic eq.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Properties:  mesh - mesh object
    %              pde - pde object
    %              space - FEM space object
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Methods   :  assembleS - assembles the curl-curl matrix S,
    %              assembleM - assemble the mass matrix M and the load
    %              assemblef - assemble the vector f
    %              solve -  applies boundary conditions and solves
    %                       the resulting linear system
    %              applyBC - apply boundary conditions
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Copyright Dimitris Kamilis, 2017
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Reference for assembly of matrices: Implementing the
    % Finite Element Assembly in interpreted languages,
    % Antti Hannukainen and Mika Juntunen
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    properties
        mesh
        pde
        space
        Ndx
        curlNdx
        glbIntx
        free
    end
    
    methods
        function obj = MaxwFEM(mesh,pde,space)
            % Constructor
            % Precompute global integration points and 
            % the corresponding basis functions
            obj.mesh = mesh;
            obj.pde = pde;
            obj.space = space;
            if isempty(obj.Ndx) && isempty(obj.curlNdx)
                lndof = obj.space.ndof;
                obj.Ndx = cell(lndof,3);
                obj.curlNdx = cell(lndof,3);
                for n=1:lndof
                    for i = 1:3
                        obj.Ndx{n,i} = obj.mesh.invBT{i}*obj.space.N{n};
                        obj.curlNdx{n,i} = (bsxfun(@rdivide,obj.mesh.B{i},obj.mesh.detB))*obj.space.curlN{n};
                    end
                end               
            end
            if isempty(obj.glbIntx)
                obj.glbIntx = obj.mesh.glbIntx(obj.space.tetquad, true);
              %  obj.glbIntx = zeros(obj.space.tetquad.nint,obj.mesh.nt,3);
              %  for i = 1:3
              %      obj.glbIntx(:,:,i) = bsxfun(@plus, obj.mesh.B{i}*obj.space.tetquad.X,obj.mesh.b{i}).';
              %  end
              %  obj.glbIntx = reshape(obj.glbIntx,obj.mesh.nt*obj.space.tetquad.nint,3);
            end
            if isempty(obj.free)
                if isempty(obj.mesh.bdFlag)
                    obj.mesh.setBC('',2);
                    warning('Assuming homogeneous Neumann boundary conditions')
                    pause
                end
                if ~isempty(obj.mesh.bdFlag)
                    if any(obj.mesh.bdFlag==1) % Dirichlet
                        obj.mesh.extraStruct();
                        idx = obj.mesh.fa2ed(obj.mesh.bdFlag == 1,:);
                        idx = unique(idx(:));
                        fixed = false(obj.mesh.ne,1);
                        fixed(idx) = true;
                        obj.free = ~fixed;
                    end
                end
            end
        end
        
        function S = assembleS(obj)
            % Assemble curl-curl bilinear form
            ndof = obj.mesh.ne; % global dof
            lndof = obj.space.ndof; % local dof
            nt = obj.mesh.nt;
            ii = zeros(nt,lndof*lndof);
            ji = ii;
            Sv = ii;
            for n=1:lndof
                for m=1:lndof
                    % TODO, non diagonal kappa
                    for i = 1:3
                        Sv(:,(n-1)*lndof+m) = Sv(:,(n-1)*lndof+m) + ...
                            obj.pde.invmu.*((obj.curlNdx{n,i}.*obj.curlNdx{m,i})*obj.space.tetquad.W.*abs(obj.mesh.detB));
                    end
                    ii(:,(n-1)*lndof+m) = obj.mesh.el2ed(:,n);
                    ji(:,(n-1)*lndof+m) = obj.mesh.el2ed(:,m);
                end
                
            end
            S = sparse(ii,ji,Sv,ndof,ndof) ;
        end
        
        function M = assembleM(obj)
            % Assemble gram matrix bilinear form
            ndof = obj.mesh.ne; % global dof
            lndof = obj.space.ndof; % local dof
            nt = obj.mesh.nt;
            ii = zeros(nt,lndof*lndof);
            ji = ii;
            Mv = ii;
            if size(obj.pde.kappa,2) > 1
                for n=1:lndof
                    for m=1:lndof
                        for i = 1:3
                            Mv(:,(n-1)*lndof+m) = Mv(:,(n-1)*lndof+m) + ...
                                (obj.pde.kappa.*obj.Ndx{n,i}.*obj.Ndx{m,i})*obj.space.tetquad.W.*abs(obj.mesh.detB);
                        end
                        ii(:,(n-1)*lndof+m) = obj.mesh.el2ed(:,n);
                        ji(:,(n-1)*lndof+m) = obj.mesh.el2ed(:,m);
                    end
                end
            elseif iscolumn(obj.pde.kappa)
                for n=1:lndof
                    for m=1:lndof
                        for i = 1:3
                            Mv(:,(n-1)*lndof+m) = Mv(:,(n-1)*lndof+m) + ...
                                obj.pde.kappa.*((obj.Ndx{n,i}.*obj.Ndx{m,i})*obj.space.tetquad.W.*abs(obj.mesh.detB));
                        end
                        ii(:,(n-1)*lndof+m) = obj.mesh.el2ed(:,n);
                        ji(:,(n-1)*lndof+m) = obj.mesh.el2ed(:,m);
                    end
                end
            else
                error('wrong obj.pde.kappa format')
            end
            M = sparse(ii,ji,Mv,ndof,ndof);
        end
               
        function f = assemblef(obj)
            % Assemble linear form
            ndof = obj.mesh.ne; % global dof
            lndof = obj.space.ndof; % local dof
            nt = obj.mesh.nt;
            fv = zeros(nt,lndof);
            fi = fv;
            
            if strcmp(obj.pde.sourceType,'incident')
                lf = obj.pde.fhat(obj.glbIntx(:,1),obj.glbIntx(:,2),obj.glbIntx(:,3));
                lfx = reshape(lf(:,1),obj.space.tetquad.nint,obj.mesh.nt).';
                lfy = reshape(lf(:,2),obj.space.tetquad.nint,obj.mesh.nt).';
                lfz = reshape(lf(:,3),obj.space.tetquad.nint,obj.mesh.nt).';
                if ismatrix(obj.pde.deltaSigma) && size(obj.pde.deltaSigma,2)>1
                    lfx = lfx.*obj.pde.deltaSigma;
                    lfy = lfy.*obj.pde.deltaSigma;
                    lfz = lfz.*obj.pde.deltaSigma;
                    dsig = ones(obj.mesh.nt,1);
                elseif isvector(obj.pde.deltaSigma)
                    dsig = obj.pde.deltaSigma(:);                
                end
                for n=1:lndof                 
                    fv(:,n) = (obj.Ndx{n,1}.*lfx + obj.Ndx{n,2}.*lfy + obj.Ndx{n,3}.*lfz)*obj.space.tetquad.W.*dsig.*abs(obj.mesh.detB);
                    fi(:,n) = obj.mesh.el2ed(:,n);
                end
                f = 1i*obj.pde.omega*accumarray(fi(:),fv(:),[ndof,1]);
            elseif strcmp(obj.pde.sourceType,'solution')
                [lfx,lfy,lfz] = obj.pde.fhat();
                lfx = lfx.';
                lfy = lfy.';
                lfz = lfz.';               
                %lfx = reshape(lf(:,1),obj.space.tetquad.nint,obj.mesh.nt).';
                %lfy = reshape(lf(:,2),obj.space.tetquad.nint,obj.mesh.nt).';
                %lfz = reshape(lf(:,3),obj.space.tetquad.nint,obj.mesh.nt).';
                if ismatrix(obj.pde.deltaSigma) && size(obj.pde.deltaSigma,2)>1
                    lfx = lfx.*obj.pde.deltaSigma;
                    lfy = lfy.*obj.pde.deltaSigma;
                    lfz = lfz.*obj.pde.deltaSigma;
                    dsig = ones(obj.mesh.nt,1);
                elseif isvector(obj.pde.deltaSigma)
                    dsig = obj.pde.deltaSigma(:);                
                end
                for n=1:lndof                 
                    fv(:,n) = ((obj.Ndx{n,1}.*lfx + obj.Ndx{n,2}.*lfy + obj.Ndx{n,3}.*lfz)*obj.space.tetquad.W).*dsig.*abs(obj.mesh.detB);
                    fi(:,n) = obj.mesh.el2ed(:,n);
                end
                f = 1i*obj.pde.omega*accumarray(fi(:),fv(:),[ndof,1]);
                
            elseif strcmp(obj.pde.sourceType,'edipolefinite')
                ne = length(obj.pde.sourceDOF);
                f = zeros(obj.mesh.ne,1);
                for e=1:ne
                    %r = obj.mesh.p(obj.mesh.edges(obj.pde.sourceDOF(e),:),:);
                    %d = r(2,:)-r(1,:);
                    %l = sqrt(sum(d.^2));
                    f(obj.pde.sourceDOF(e)) = 1i*obj.pde.omega*obj.pde.fhat;
                end
                
            elseif strcmp(obj.pde.sourceType,'edipole')
                [~,x,y,z,id] = obj.mesh.toBarycentric(obj.pde.sourcePos);
                % go back to reference
                N = cell(6,1);
                N{1} = [1 - y - z ; x ; x];
                N{2} = [y ; 1-x-z ; y];
                N{3} = [z ;z ; 1-x-y];
                N{4} = [-y ; x ; 0];
                N{5} = [-z ; 0 ; x];
                N{6} = [0 ; -z  ; y];
                gN = zeros(6,3);
                % map from ref to global
                for n=1:6
                    for i = 1:3
                        gN(n,i) = obj.mesh.invBT{i}(id,:)*N{n};
                    end
                end
                %f = zeros(obj.mesh.ne,1);
                f = spalloc(obj.mesh.ne,1,6);
                f(obj.mesh.el2ed(id,:)) = 1i*obj.pde.omega*sum(bsxfun(@times,gN,obj.pde.fhat),2);           
            elseif strcmp(obj.pde.sourceType,'vector')
                % Assemble gram matrix bilinear form
                ndof = obj.mesh.ne; % global dof
                lndof = obj.space.ndof; % local dof
                nt = obj.mesh.nt;
                ii = zeros(nt,lndof*lndof);
                ji = ii;
                Mv = ii;
                if ismatrix(obj.pde.deltaSigma)
                    dsig = obj.pde.deltaSigma(:);
                elseif ndims(obj.pde.deltaSigma) == 3
                    dsig = squeeze(obj.pde.deltaSigma(1,1,:));% non-tensor sigma TODO!
                end
                for n=1:lndof
                    for m=1:lndof
                        % watch out for pde.kappa here. does it work for
                        % non-diagonal kappa? TODO!!!!!!!!!
                        for i = 1:3
                            for q = 1:3
                                Mv(:,(n-1)*lndof+m) = Mv(:,(n-1)*lndof+m) + ...
                                   dsig.*((obj.Ndx{n,i}.*obj.Ndx{m,i})*obj.space.tetquad.W.*abs(obj.mesh.detB));
                            end
                        end
                        ii(:,(n-1)*lndof+m) = obj.mesh.el2ed(:,n);
                        ji(:,(n-1)*lndof+m) = obj.mesh.el2ed(:,m);
                    end
                end
                M = sparse(ii,ji,Mv,ndof,ndof);
                f = 1j*obj.pde.omega*M*obj.pde.fhat;
            elseif strcmp(obj.pde.sourceType,'edipoleReg')
                % find id of cell 
                [~,x,y,z,id] = obj.mesh.toBarycentric(obj.pde.sourcePos);
                % find neighbouring cells
                TR = triangulation(double(obj.mesh.t), obj.mesh.p);
                neighs = TR.neighbors(id);
                % define regularization function 
                eta25 = @(r) 105*(1-21*r^2+56*r^3-54*r^4+18*r^5)/pi;  
                [inradius,incenter,outradius,outcenter,~] = obj.mesh.calcTetProp(id); %local mesh size ~ outradius
                H = outradius;
                lndof = obj.space.ndof;
                Ndx = cell(lndof,3);
                nmcp = 1000000;
                intxmc = zeros(nmcp,1);
                iN = zeros(6,1);
                xs = obj.pde.sourcePos(1);
                ys = obj.pde.sourcePos(2);
                zs = obj.pde.sourcePos(3);
                xmc = (tetrahedron01_sample(nmcp,20349));
                tetmc = Quadrature(3,1);
                tetmc.nint = nmcp;
                tetmc.X = xmc;
                basis = NCurl(tetmc);
               % H = inradius;
               %xs = incenter(1);
               % ys = incenter(2);
               % zs = incenter(3);
                % get faces of tetrahedron
                %[ distances, surface_points ] = point2trimesh( 'Faces',int32(obj.mesh.faces), 'Vecrtices',obj.mesh.p, 'QueryPoints',obj.pde.sourcePos);
                %H = abs(distances);
                % ratio of tet vol to sphere vol
                % loop over neighs
                jh = @(x,y,z) [0,0,0] + (norm([x-xs,y-ys,z-zs])<=H) * obj.pde.fhat * eta25(norm([x-xs,y-ys,z-zs])/H)/H^3; 
                for k=1:length(neighs)
                    neigh_id = neighs(k);
                    tetvol = (abs(obj.mesh.detB(neigh_id))/6);
               % sphvol = 4*pi*inradius^3/3;
               % rat = tetvol/sphvol;     
                %spheq = @(x,y,z)  (x-xs)^2 + (y-ys)^2 + (z-zs)^2 <= inradius^2;                                                
                    % Monte carlo integration points at neigh cell neigh_id                    
                    for i = 1:3
                        intxmc(:,i) = bsxfun(@plus, obj.mesh.B{i}(neigh_id,:)*xmc,obj.mesh.b{i}(neigh_id,:)).';
                    end  
                    % Evaluate basis functions
                    for n=1:lndof
                        for i = 1:3
                            Ndx{n,i} = obj.mesh.invBT{i}(neigh_id,:)*basis.N{n};                        
                        end
                    end 
                    lf = arrayfun(jh,intxmc(:,1),intxmc(:,2),intxmc(:,3),'UniformOutput',false);
                    lf = cell2mat(lf);
                    lfx = reshape(lf(:,1),nmcp,1).';
                    lfy = reshape(lf(:,2),nmcp,1).';
                    lfz = reshape(lf(:,3),nmcp,1).';                
                    for n=1:lndof                 
                        iN(n) = sum((Ndx{n,1}.*lfx + Ndx{n,2}.*lfy + Ndx{n,3}.*lfz),2)/nmcp;
                        iN(n) = iN(n)*tetvol;
                    end
                    f = spalloc(obj.mesh.ne,1,6);
                    f(obj.mesh.el2ed(neigh_id,:)) = f(obj.mesh.el2ed(neigh_id,:)) + 1i*obj.pde.omega*iN;    
                end
            else
                error('Invalid source type')
            end
        end
        
        function Q = obsOpReg(obj,rpos, rdip)
            l = size(rpos,1);
            Q = spalloc(obj.mesh.ne,l,l*6);
            nmcp = 1000000;
            intxmc = zeros(nmcp,1);
            xmc = (tetrahedron01_sample(nmcp,20349));
            tetmc = Quadrature(3,1);
            tetmc.nint = nmcp;
            tetmc.X = xmc;
            basis = NCurl(tetmc);
            iN = zeros(6,1);
            eta25 = @(r) 105*(1-21*r^2+56*r^3-54*r^4+18*r^5)/pi; 
            TR = triangulation(double(obj.mesh.t), obj.mesh.p);
            lndof = obj.space.ndof;
            Ndx = cell(lndof,3);
            lf = zeros(nmcp,3);
            for ipos=1:l              
                pos = rpos(ipos,:);
                [~,x,y,z,id] = obj.mesh.toBarycentric(pos);
                neighs = TR.neighbors(id);
                % [ distances, surface_points ] = point2trimesh( 'Faces',int32(obj.mesh.faces), 'Vecrtices',obj.mesh.p, 'QueryPoints', pos);
               % H = abs(distances);
                [inradius,incenter,outradius,outcenter,~] = obj.mesh.calcTetProp(id); 
                H = outradius;
                xs = pos(1);
                ys = pos(2);
                zs = pos(3);
                for k=1:length(neighs)
                    neigh_id = neighs(k);              
                %xs = incenter(1);
                %ys = incenter(2);
                %zs = incenter(3);
                % define regularization function
                    tetvol = (abs(obj.mesh.detB(neigh_id))/6);
                %sphvol = 4*pi*inradius^3/3;                
              %  jh = @(x,y,z) [0,0,0] + (norm([x-xs,y-ys,z-zs])<=H) * rdip(ipos,:) * eta23(norm([x-xs,y-ys,z-zs])/H)/H^3;
                % get int points in cell id
                    for i = 1:3
                        intxmc(:,i) = bsxfun(@plus, obj.mesh.B{i}(neigh_id,:)*xmc,obj.mesh.b{i}(neigh_id,:)).';
                    end               
                    for n=1:lndof
                        for i = 1:3
                            Ndx{n,i} = obj.mesh.invBT{i}(neigh_id,:)*basis.N{n};
                        end
                    end
                    distxmc = sqrt((intxmc(:,1)-xs).^2 + (intxmc(:,2)-ys).^2 + (intxmc(:,3)-zs).^2);
                    insideH = (distxmc<=H);              
                    for idip=1:3
                        lf(insideH,:) = eta25(distxmc(insideH)/H)/H^3 * rdip(idip,:);             
                        for n=1:lndof
                            iN(n) = sum((Ndx{n,1}.*lf(:,1).' + Ndx{n,2}.*lf(:,2).' + Ndx{n,3}.*lf(:,3).'),2)/nmcp;
                            iN(n) = iN(n)*tetvol;
                        end               
                        Q(obj.mesh.el2ed(neigh_id,:),ipos + (idip-1)*l) =  Q(obj.mesh.el2ed(neigh_id,:),ipos + (idip-1)*l) + iN;
                    end
                end
            end
            Q=Q.';
        end
        
        function [xi, free, varargout] = solve(obj,S,M,f,varargin)
            % Apply boundary conditions and solve system
            if nargin>4
                method = varargin{1};
            else
                method = 'direct';
            end
            ndof = obj.mesh.ne;
            if isempty(obj.mesh.bdFlag)
                obj.mesh.setBC('',2);
                warning('Assuming homogeneous Neumann boundary conditions')
                pause
            end
            if ~isempty(obj.mesh.bdFlag)
                if any(obj.mesh.bdFlag==2) && isa(obj.pde.gN,'function_handle')
                    neumidx = obj.mesh.bdFlag == 2;
                    obj.bdIntegral(obj.pde.gN,obj.mesh.p,obj.mesh.faces(neumidx,:),obj.mesh.detB(obj.mesh.fa2el(neumidx)));
                end
                if any(obj.mesh.bdFlag==1) % Dirichlet
                    obj.mesh.extraStruct();
                    idx = obj.mesh.fa2ed(obj.mesh.bdFlag == 1,:);
                    idx = unique(idx(:));
                    fixed = false(obj.mesh.ne,1);
                    fixed(idx) = true;
                    free = ~fixed;
                    xi = zeros(ndof,1);
                    if isa(obj.pde.gD,'function_handle')
                        xi(fixed) = obj.edInterp(obj.pde.gD,obj.mesh.p,obj.mesh.edges(fixed,:));
                        f = f(free) - (S(free,fixed)+M(free,fixed))*xi(fixed); % modify load vector
                    elseif obj.pde.gD == 0
                        f = f(free);
                    else
                        error('Wrong value of gD')
                    end
                    if strcmp(method,'direct')                  
                        xi(free) = (S(free,free)+M(free,free))\f;
                        fprintf('Time for solution:%d\n',toc)
                    elseif strcmp(method,'iterative')
                        error('Iterative method not implemented')
                    end
                else
                    if strcmp(method,'direct')
                        xi = (S+M)\f;
                    elseif strcmp(method,'iterative')
                        error('Iterative method not implemented')
                    end
                end
            end
        end

        function [S, M, f, xi, free] = applyBC(obj,S,M,f)
            % Apply boundary conditions and return solution and matrices
            ndof = obj.mesh.ne;
            xi = zeros(ndof,1);
            if isempty(obj.mesh.bdFlag)
                obj.mesh.setBC('',2);
                warning('Assuming homogeneous Neumann boundary conditions')
                pause
            end
            if ~isempty(obj.mesh.bdFlag)
%                 if any(obj.mesh.bdFlag==2) && isa(obj.pde.gN,'function_handle')
%                     neumidx = obj.mesh.bdFlag == 2;
%                     obj.bdIntegral(obj.pde.gN,obj.mesh.p,obj.mesh.faces(neumidx,:),obj.mesh.detB(obj.mesh.fa2el(neumidx)));
%                 end
                if any(obj.mesh.bdFlag==1) % Dirichlet
                    obj.mesh.extraStruct();
                    idx = obj.mesh.fa2ed(obj.mesh.bdFlag == 1,:);
                    idx = unique(idx(:));
                    fixed = false(obj.mesh.ne,1);
                    fixed(idx) = true;
                    free = ~fixed;
                    if isa(obj.pde.gD,'function_handle')
                        xi(fixed) = obj.edInterp(obj.pde.gD,obj.mesh.p,obj.mesh.edges(fixed,:));
                        f = f(free) - (S(free,fixed)+M(free,fixed))*xi(fixed); % modify load vector
                    elseif obj.pde.gD == 0
                        f = f(free);
                    else
                        error('Wrong value of gD')
                    end
                    S = S(free,free);
                    M = M(free,free);
                end
            end
        end
        
        function obj = set.pde(obj, pde)
            if isa(pde,'MaxwPDE')
                obj.pde = pde;
            else 
                error('pde must be of MaxwPDE class')
            end
        end
        
    end %methods
      
    methods (Static)
        function fI = edInterp(func,nodes,edges)
            % Compute the interpolant (r_h u) of func with Gauss-Legendre
            % quadrature according to int_e (u-r_h u)\cdot t ds = 0
            ne = size(edges,1);
            tang = nodes(edges(:,2),:) - nodes(edges(:,1),:); % tangents
            rt = @(tq) tang*tq + nodes(edges(:,1),:); % parametrization
            integrand = @(tq) func(rt(tq));
            fI = GaussLegendre(integrand,0,1,2);
            fI = sum(tang.*fI,2);
            assert(isequal(size(fI),[ne 1]));
        end
    end % methods(Static)
end
