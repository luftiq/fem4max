classdef (Abstract) Element < handle
    % Abstract finite element class
    % Copyright Dimitris Kamilis 2017
    properties (Abstract, SetAccess = private)
        N
        tetquad
        order
        ndof
    end       
end