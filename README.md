# fem4max
Copyright Dimitris Kamilis 2017

Finite Element Method package to solve the time-harmonic Maxwell equations in 3D using the linear Nedelec edge elements on tetrahedra
$$
\nabla(\mu^{-1}\nabla\times E)-\imath\omega(\sigma-\imath\omega\epsilon) E = \imath \omega j
$$

## Quick-start
Run example.mlx for a basic example

## Contents
- Tetmesh: class that handles mesh information
- MaxwPDE: class that handles parameters for Maxwell's equations
- MaxwFEM: class that handles the assembly of the FEM matrices
- Post: post-processing class
- Quadrature: class that handles quadrature rules
- NCurl, NDiv, NGrad: FEM basis for lowest-order edge elements, face elements and node elements respectively.
- FemMat: FEM toolbox class that can assemble additional forms
- QuadRules.csv, QuadRulesTet.mat: definitions of quadrature rules
- Jacobian: class to calculate Jacobian
--------------------------------------------------------------------
- External Tools: external libraries needed and auxiliary tools
- Mesh: tools for mesh manipulation
- CSEM: CSEM inversion scripts

## Basic instructions
1. Start by using Tetmesh to load a mesh
2. Define a quad rule using Quadrature
3. Define the basis functions using NCurl
4. Set the PDE parameters using MaxPDE
5. Set the FEM system using MaxFEM
6. Assemble the FEM matrices and solve the system
7. Post-process using Post