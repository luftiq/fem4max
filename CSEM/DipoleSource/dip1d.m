function fh = makedip1d(sPos,sDip,azimuth,dip,freq,layers)
paren = @(x, varargin) x(varargin{:}); % Helper function
fh = @dip1d;
%Hash = DataHash([sPos sDip azimuth dip freq]); % Data hash
%store = MapN();
    function dip1d(x,y,z)
        [Ex,Ey,Ez] = sDip*paren(mexDipole1D([sPos,azimuth,dip],freq,layers,[x y z]),:,4:6);
    end
end