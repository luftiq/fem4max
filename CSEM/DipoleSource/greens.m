function G = greens(x)

I0 = 1;
dl = 1;
f = 0.25;
omega = 2*pi*f;
mu0 = 4*pi*1e-7;
sigma_b=1;

k = sqrt(1i*omega*mu0*sigma_b);
R = norm(x);
g = 1i*omega*mu0*I0*dl*exp(1i*k*R)/(4*pi*R);

E = 1/(k^2)*1/(R*R)*g;
Et = E*(k*k*R*R + 1i*k*R - 1);
Ert = E*(-3*1i*k - 3/R - R*k*k) *(1/R);

Exx = Ert*x(1)*x(1) + Et;
Exy = Ert*x(1)*x(2);
Exz = Ert*x(1)*x(3);
Eyx = Exy;
Eyy = Ert*x(2)*x(2) + Et;
Eyz = Ert*x(2)*x(3);
Ezx = Exz;
Ezy = Eyz;
Ezz = Ert*x(3)*x(3) + Et;
G = [Exx Exy Exz;Eyx Eyy Eyz;Ezx Ezy Ezz];
