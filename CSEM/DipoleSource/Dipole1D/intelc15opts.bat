@echo off
rem INTELC15OPTS.BAT
rem
rem    autor:i3v
rem    Homebrew mexopts for icc15 (Intel� Parallel Studio XE 2015)
rem    based on original matlab options file for icc13.
rem
rem    Although intel's linker (xilink) is used instead of Microsoft's 
rem    (original mexopts, created by Mathworks always use Microsoft's 
rem    linker for some reason), Microsoft Visual Studio 2013 (VS12) 
rem    is also required.
rem 
rem    A list of icc versions is publiched here:
rem    https://software.intel.com/en-us/articles/intel-compiler-and-composer-update-version-numbers-to-compiler-version-number-mapping
rem
rem    A list of MSVS version is published here:
rem    https://en.wikipedia.org/w/index.php?title=Microsoft_Visual_Studio&oldid=679499819#History
rem
rem
rem StorageVersion: 1.0
rem C++keyFileName: INTELC15OPTS.BAT
rem C++keyName: Intel C++
rem C++keyManufacturer: Intel
rem C++keyVersion: 15.0
rem C++keyLanguage: C++
rem C++keyLinkerName: Intel xilink
rem C++keyLinkerVersion: 15.0
rem
rem I don't actually know "keyLinkerVersion" .... maybe it's 13.0, maybe it's 9999.9, though, it's from Composer XE-2013 SP1.
rem
rem The original file, created by Mathworks is:
rem    $Revision: 1 $  $Date: 2014/05/09 17:21 $rem
rem ********************************************************************
rem General parameters
rem ********************************************************************
set MATLAB=%MATLAB%
set ICPP_COMPILER15=%IFORT_COMPILER16%
set VSINSTALLDIR=%VS140COMNTOOLS%\..\..
set VCINSTALLDIR=%VSINSTALLDIR%\VC
set LINKERDIR=IFORT_COMPILER16
rem In this case, SDKDIR is being used to specify the location of the SDK
set SDKDIR='.registry_lookup("SOFTWARE\Microsoft\Microsoft SDKs\Windows\v8.1A" , "InstallationFolder").'
set PATH=%IFORT_COMPILER16%\Bin\Intel64;%VCINSTALLDIR%\bin\amd64;%SDKDIR%\bin\x64;%VCINSTALLDIR%\BIN;%VSINSTALLDIR%\Common7\Tools;%VSINSTALLDIR%\Common7\Tools\bin;%MATLAB_BIN%;%PATH%
set INCLUDE=%IFORT_COMPILER16%\compiler\Include;%SDKDIR%\include;%VCINSTALLDIR%\ATLMFC\INCLUDE;%VCINSTALLDIR%\INCLUDE;%VCINSTALLDIR%\VCPackages;%INCLUDE%
set LIB=%IFORT_COMPILER16%\compiler\Lib\Intel64;%SDKDIR%\LIB\x64;%VCINSTALLDIR%\LIB\amd64;%VCINSTALLDIR%\ATLMFC\LIB\amd64;%MATLAB%\extern\lib\win64;%LIB%
set MW_TARGET_ARCH=win64

rem ********************************************************************
rem Compiler parameters
rem ********************************************************************
set COMPILER=icl
set COMPFLAGS=/c /GR /W3 /EHs /D_CRT_SECURE_NO_DEPRECATE /D_SCL_SECURE_NO_DEPRECATE /D_SECURE_SCL=0 /DMATLAB_MEX_FILE /nologo /MD
set OPTIMFLAGS=/O2 /DNDEBUG
set DEBUGFLAGS=/Z7
set NAME_OBJECT=/Fo

rem ********************************************************************
rem Linker parameters
rem ********************************************************************
set LIBLOC=%MATLAB%\extern\lib\win64\microsoft
set LINKER=xilink
set LINKFLAGS=/dll /export:%ENTRYPOINT% /LIBPATH:"%LIBLOC%" libmx.lib libmex.lib libmat.lib /MACHINE:X64 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /manifest /incremental:NO /implib:"%LIB_NAME%.x" /MAP:"%OUTDIR%%MEX_NAME%%MEX_EXT%.map"
set LINKOPTIMFLAGS=
set LINKDEBUGFLAGS=/debug /PDB:"%OUTDIR%%MEX_NAME%%MEX_EXT%.pdb"
set LINK_FILE=
set LINK_LIB=
set NAME_OUTPUT=/out:"%OUTDIR%%MEX_NAME%%MEX_EXT%"
set RSP_FILE_INDICATOR=@

rem ********************************************************************
rem Resource compiler parameters
rem ********************************************************************
set RC_COMPILER=rc /fo "%OUTDIR%mexversion.res"
set RC_LINKER=

set POSTLINK_CMDS=del "%LIB_NAME%.x" "%LIB_NAME%.exp"
set POSTLINK_CMDS1=mt -outputresource:"%OUTDIR%%MEX_NAME%%MEX_EXT%";2 -manifest "%OUTDIR%%MEX_NAME%%MEX_EXT%.manifest"
set POSTLINK_CMDS2=del "%OUTDIR%%MEX_NAME%%MEX_EXT%.manifest" 
set POSTLINK_CMDS3=del "%OUTDIR%%MEX_NAME%%MEX_EXT%.map"
