function Ep = dipoleHom(dip,sPos,omega,mu,sigma,epsilon,x,y,z)
% Calculates the electric field Ep for a point dipole in a homogeneous
% background medium

%   Copyright 2017 Dimitris Kamilis
% Input: dip - source dipole moment vector
%        sPos - source position vector
%        omega - angular frequency
%        mu - permeability
%        sigma - background homogeneous conductivity 
%        epsilon - permittivity
%        x,y,z - location to compute field 

x0 = sPos(1);
y0 = sPos(2);
z0 = sPos(3);
r = sqrt((x-x0).^2+(y-y0).^2+(z-z0).^2);
Ep = complex(zeros(length(r),3));
k =  sqrt(omega^2*mu*epsilon + 1i*omega*mu*sigma); %k = omega*sqrt(epsilon mu) with complex epsilon = epsilon' + i omega /sigma
nx = (x-x0)./r;
ny = (y-y0)./r;
nz = (z-z0)./r;
njs = nx*dip(1) + ny*dip(2) + nz*dip(3);
Z = omega*mu/k;
ikr = 1i*k*r;
temp = exp(ikr);
Ep(:,1) = Z*temp.*( ( dip(1) - njs.*nx).*ikr + (dip(1) - 3*njs.*nx).*(1./(ikr)-1))./(4*pi*r.^2);
Ep(:,2) = Z*temp.*( ( dip(2) - njs.*ny).*ikr + (dip(2) - 3*njs.*ny).*(1./(ikr)-1))./(4*pi*r.^2);
Ep(:,3) = Z*temp.*( ( dip(3) - njs.*nz).*ikr + (dip(3) - 3*njs.*nz).*(1./(ikr)-1))./(4*pi*r.^2);

% for quasistatic
% factor = exp(ikr)./(4*pi*sigma*r.^3);
% pr = dip(1)*(x-x0) + dip(2)*(y-y0) + dip(3)*(z-z0);
% Ep(:,1) = factor.*((-1+ikr+k^2*r.^2)*dip(1) + (3-3*ikr-k^2*r.^2).*pr.*(x-x0)./r.^2);
% Ep(:,2) = factor.*((-1+ikr+k^2*r.^2)*dip(2) + (3-3*ikr-k^2*r.^2).*pr.*(y-y0)./r.^2);
% Ep(:,3) = factor.*((-1+ikr+k^2*r.^2)*dip(3) + (3-3*ikr-k^2*r.^2).*pr.*(z-z0)./r.^2);
