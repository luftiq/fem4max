function [contam,randv] = addnoise2(data, std)
randv = randn(size(data));
if isreal(data)
    std = std*(abs(data));
    noise = randv.*std;
    contam = data + noise;
else
    std = std*(mean(data));
    noise = sqrt(1/2)*(randn(size(data)).*std + 1j*randn(size(data)).*std);
    contam = data + noise;
end