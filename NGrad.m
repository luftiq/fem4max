classdef NGrad < Element
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Class for linear nodal basis functions
    % The basis functions and their grad are defined on the
    % reference tetrahedron
    % Properties: N{i} where i=1,...,4 containt the lowest order nodal
    %             basis functions for the reference tetrahedron evaluated
    %             at the quadrature points defined by the input object
    %             'tetquad'
    %             gradN{i} where i=1,...,4 contains the corresponding grad
    %             tetquad - an instance of the quadrature class for 3D 
    %             tetrahedra. Contains the integration points and weights
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Copyright Dimitris Kamilis, 2017
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    properties (SetAccess = private)
        N
        gradN
        tetquad
        order = 1;
        ndof = 4; % number of degrees of freedom
        type = 'NGrad';
    end
    
    methods
        function obj = NGrad(tetquad)
            if tetquad.dim~=3 
                error('Quadrature rule for tetraheda needed')
            end
            obj.tetquad = tetquad;
            X = tetquad.X;
            obj.N = cell(obj.ndof,1);
            obj.gradN = cell(obj.ndof,1);
            obj.N{1} = 1-X(1,:)-X(2,:)-X(3,:);
            obj.N{2} = X(1,:);
            obj.N{3} = X(2,:);
            obj.N{4} = X(3,:);
            obj.gradN{1} = repmat([-1 -1 -1]',1,tetquad.nint);
            obj.gradN{2} = repmat([1 0 0]',1,tetquad.nint);
            obj.gradN{3} = repmat([0 1 0]',1,tetquad.nint);
            obj.gradN{4} = repmat([0 0 1]',1,tetquad.nint);
        end         
    end   
end