classdef Post
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Class for post-processing a solution
    % e.g. interpolation of solution to nodes
    % and export to .vtu format
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Copyright Dimitris Kamilis, 2017
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties
        maxw
        quad
        sol % Numerical solution
    end
    
    methods
        function obj = Post(maxw,quad,sol)
            obj.maxw = maxw;
            obj.quad = quad;
            obj.sol = sol;
        end
        
        function [Ex,Ey,Ez,psub] = solInterp(obj)
            % Interpolate solution at quadrature points            
            % Barycentric coordinates
            coords = obj.quad.X.';
            coords(:,4) = ones(obj.quad.nint,1)-sum(coords,2);
%             p1 = obj.maxw.mesh.p(obj.maxw.mesh.t(:,2),:);
%             p2 = obj.maxw.mesh.p(obj.maxw.mesh.t(:,3),:);
%             p3 = obj.maxw.mesh.p(obj.maxw.mesh.t(:,4),:);
%             p4 = obj.maxw.mesh.p(obj.maxw.mesh.t(:,1),:);
%             psub = kron(p1,coords(:,1)) + kron(p2,coords(:,2)) + kron(p3,coords(:,3)) + ...
%                 kron(p4,coords(:,4)); 
            psub = obj.maxw.glbIntx;            
            [~,b,c,d] = obj.calcHatGradients(); % Gradients          
            % Solution values            
            sol1 = obj.sol(obj.maxw.mesh.el2ed(:,1));
            sol2 = obj.sol(obj.maxw.mesh.el2ed(:,2));
            sol3 = obj.sol(obj.maxw.mesh.el2ed(:,3));
            sol4 = obj.sol(obj.maxw.mesh.el2ed(:,4));
            sol5 = obj.sol(obj.maxw.mesh.el2ed(:,5));
            sol6 = obj.sol(obj.maxw.mesh.el2ed(:,6));           
            % Field values (nt X ne)
            Ex = coords(:,1)*(sol1.*b(:,2) + sol2.*b(:,3) + sol3.*b(:,4)).' + ...
                coords(:,2)*(-sol1.*b(:,1) + sol4.*b(:,3) + sol5.*b(:,4)).' + ...
                coords(:,3)*(-sol2.*b(:,1) + sol6.*b(:,4) - sol4.*b(:,2)).' + ...
                coords(:,4)*(-sol3.*b(:,1) - sol5.*b(:,2) - sol6.*b(:,3)).';
            
            Ey = coords(:,1)*(sol1.*c(:,2) + sol2.*c(:,3) + sol3.*c(:,4)).' + ...
                coords(:,2)*(-sol1.*c(:,1) + sol4.*c(:,3) + sol5.*c(:,4)).' + ...
                coords(:,3)*(-sol2.*c(:,1) + sol6.*c(:,4) - sol4.*c(:,2)).' + ...
                coords(:,4)*(-sol3.*c(:,1) - sol5.*c(:,2) - sol6.*c(:,3)).';
            
            Ez = coords(:,1)*(sol1.*d(:,2) + sol2.*d(:,3) + sol3.*d(:,4)).' + ...
                coords(:,2)*(-sol1.*d(:,1) + sol4.*d(:,3) + sol5.*d(:,4)).' + ...
                coords(:,3)*(-sol2.*d(:,1) + sol6.*d(:,4) - sol4.*d(:,2)).' + ...
                coords(:,4)*(-sol3.*d(:,1) - sol5.*d(:,2) - sol6.*d(:,3)).'; 
        end
        
        function Q = sol2Position2(obj,pos,obs,free)
            % Construct interpolator operator Q for selected points
            l = size(pos,1);
            [~,b,c,d] = obj.calcHatGradients(); % Gradients
            dofs = zeros(l, obj.maxw.space.ndof);
            vx = zeros(l, obj.maxw.space.ndof);
            vy = vx;
            vz = vx;
            for i=1:l
                [u1,u2,u3,u4,id] = obj.maxw.mesh.toBarycentric(pos(i,:));
                assert(length(id)==1)
                dofs(i,:) = obj.maxw.mesh.el2ed(id,:);                                              
                vx(i,:) = [u1*b(id,2)-u2*b(id,1),u1*b(id,3)-u3*b(id,1),u1*b(id,4)-u4*b(id,1), ...
                    u2*b(id,3)-u3*b(id,2),u2*b(id,4)-u4*b(id,2),u3*b(id,4)-u4*b(id,3)];
                                   
                vy(i,:) = [u1*c(id,2)-u2*c(id,1),u1*c(id,3)-u3*c(id,1),u1*c(id,4)-u4*c(id,1), ...
                    u2*c(id,3)-u3*c(id,2),u2*c(id,4)-u4*c(id,2),u3*c(id,4)-u4*c(id,3)];
                                              
                vz(i,:) = [u1*d(id,2)-u2*d(id,1),u1*d(id,3)-u3*d(id,1),u1*d(id,4)-u4*d(id,1), ...
                    u2*d(id,3)-u3*d(id,2),u2*d(id,4)-u4*d(id,2),u3*d(id,4)-u4*d(id,3)];                
            end
            rowind = repmat(1:3*l,1,size(vx,2));
            colind = double([dofs;dofs;dofs]);
            Q = sparse(rowind, colind, [vx;vy;vz], 3*l, obj.maxw.mesh.ne);
            Q = Q(:,free);
            
        end
        
        function [Qx,Qy,Qz] = sol2Position(obj,pos)
            % Construct interpolator operator Q for selected points
            % TODO: vectorize
            l = size(pos,1);
            [~,b,c,d] = obj.calcHatGradients(); % Gradients
            Qx = zeros(l,obj.maxw.mesh.ne);
            Qy = Qx;
            Qz = Qx;           
            for i=1:l
                [u1,u2,u3,u4,id] = obj.maxw.mesh.toBarycentric(pos(i,:));    
                assert(length(id)==1)               
                dofs = obj.maxw.mesh.el2ed(id,:);
                Qx(i,dofs) = [u1*b(id,2)-u2*b(id,1),u1*b(id,3)-u3*b(id,1),u1*b(id,4)-u4*b(id,1), ...
                              u2*b(id,3)-u3*b(id,2),u2*b(id,4)-u4*b(id,2),u3*b(id,4)-u4*b(id,3)];
                Qy(i,dofs) = [u1*c(id,2)-u2*c(id,1),u1*c(id,3)-u3*c(id,1),u1*c(id,4)-u4*c(id,1), ...
                              u2*c(id,3)-u3*c(id,2),u2*c(id,4)-u4*c(id,2),u3*c(id,4)-u4*c(id,3)];
                Qz(i,dofs) = [u1*d(id,2)-u2*d(id,1),u1*d(id,3)-u3*d(id,1),u1*d(id,4)-u4*d(id,1), ...
                              u2*d(id,3)-u3*d(id,2),u2*d(id,4)-u4*d(id,2),u3*d(id,4)-u4*d(id,3)];
            end                   
        end
               
        function plotSol(obj,type)
            % Plot solution using interpolated values    
             axis equal
            if strcmp(type,'nodes')
                [Ex,Ey,Ez] = obj.solNode;
                quiver3(obj.maxw.mesh.p(:,1),obj.maxw.mesh.p(:,2),obj.maxw.mesh.p(:,3),real(Ex(:)),real(Ey(:)),real(Ez(:)));  
            elseif strcmp(type,'quad')
                [Ex,Ey,Ez,psub] = obj.solInterp();
                quiver3(psub(:,1),psub(:,2),psub(:,3),real(Ex(:)),real(Ey(:)),real(Ez(:)));  
            else
                error('wrong type')
            end             
        end
        
        function [Ex,Ey,Ez] = solNode(obj,varargin)
            % Projection of solution onto nodes
            space = NGrad(obj.quad);
            ndofs = obj.maxw.mesh.np; % scalar global ndof
            ndofv = obj.maxw.mesh.ne; % vector global ndof
            lndofs = space.ndof; % local scalar dof
            lndofv = obj.maxw.space.ndof; % local vector dof
            nt = obj.maxw.mesh.nt;
            theta_P = zeros(lndofs,lndofs);
            theta_Q = zeros(3,lndofs,lndofv);
            ii = zeros(nt,lndofs*lndofs);
            ji = ii;
            Sv = ii;
            iMi = zeros(nt,lndofs*lndofv);
            jMi = zeros(nt,lndofs*lndofv);
            Mv = zeros(3,nt,lndofs*lndofv);
            
            for i=1:lndofs
                for j=1:lndofs
                    theta_P(i,j) = (space.N{i}.*space.N{j})*obj.quad.W;
                end
            end
            
            for i=1:lndofs
                for j=1:lndofv
                    for k=1:3
                        theta_Q(k,i,j) = (space.N{i}.*obj.maxw.space.N{j}(k,:))*obj.quad.W;
                    end
                end
            end
            for i=1:lndofs  % local degrees of freedom on ref. elem
                for j=1:lndofs % local degrees of freedom on ref. elem
                    Sv(:,(i-1)*lndofs+j) = Sv(:,(i-1)*lndofs+j) + ...
                        abs(obj.maxw.mesh.detB)* theta_P(i,j);
                    % indexing
                    ii(:,(i-1)*lndofs+j) = obj.maxw.mesh.t(:,i);
                    ji(:,(i-1)*lndofs+j) = obj.maxw.mesh.t(:,j);
                end
            end
            S = sparse(ii,ji,Sv,ndofs,ndofs) ;
            
            for i=1:lndofs  % local degrees of freedom on ref. elem
                for j=1:lndofv % local degrees of freedom on ref. elem
                    for r=1:3
                        for k=1:3
                            Mv(r,:,(i-1)*lndofv+j) = Mv(r,:,(i-1)*lndofv+j) + ...
                                ((obj.maxw.mesh.invBT{r}(:,k).*abs(obj.maxw.mesh.detB))*theta_Q(k,i,j)).';
                        end
                    end
                    % indexing
                    iMi(:,(i-1)*lndofv+j) = obj.maxw.mesh.t(:,i);
                    jMi(:,(i-1)*lndofv+j) = obj.maxw.mesh.el2ed(:,j);
                end
            end
            Mx = sparse(iMi,jMi,squeeze(Mv(1,:,:)),ndofs,ndofv);
            My = sparse(iMi,jMi,squeeze(Mv(2,:,:)),ndofs,ndofv);
            Mz = sparse(iMi,jMi,squeeze(Mv(3,:,:)),ndofs,ndofv);
            Ex = S\(Mx*obj.sol);
            Ey = S\(My*obj.sol);
            Ez = S\(Mz*obj.sol);
            if nargin > 1
                if nargin > 2
                    Ep = varargin{1};
                    Ex = Ex(:);Ey=Ey(:);Ez=Ez(:);
                    E = Ep(obj.maxw.mesh.p(:,1),obj.maxw.mesh.p(:,2),obj.maxw.mesh.p(:,3));
                    Ex = Ex+E(:,1);Ey = Ey+E(:,2);Ez = Ez+E(:,3);
                end
                
                clear Sv Mv ii ji Mx My Mz iMi jMi
                fid = fopen(strcat(filename,'.msh.vtu'), 'w');
                fprintf(fid, '<?xml version="1.0"?>\n');
                fprintf(fid, '<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">\n');
                fprintf(fid, '<UnstructuredGrid>\n');
                fprintf(fid, '<Piece NumberOfPoints="%u" NumberOfCells="%u">\n',obj.maxw.mesh.np,obj.maxw.mesh.nt);
                fprintf(fid, '<PointData>\n');
                fprintf(fid, '<DataArray type="Float64" Name="ElectricFieldReal" NumberOfComponents="3" format="ascii">\n');
                fprintf(fid, '%6.15e %6.15e %6.15e ',[real(Ex(:)),real(Ey(:)),real(Ez(:))]');
                fprintf(fid, '\n</DataArray>\n');
                fprintf(fid, '<DataArray type="Float64" Name="ElectricFieldIm" NumberOfComponents="3" format="ascii">\n');
                fprintf(fid, '%6.15e %6.15e %6.15e ',[imag(Ex(:)),imag(Ey(:)),imag(Ez(:))]');
                fprintf(fid, '\n</DataArray>\n');
                fprintf(fid, '</PointData>\n');
                fprintf(fid, '<Points>\n');
                fprintf(fid, '<DataArray type="Float64" Name="position" NumberOfComponents="3" format="ascii">\n');
                fprintf(fid, '%6.12f %6.12f %6.12f ',obj.maxw.mesh.p.');
                fprintf(fid, '\n</DataArray>\n');
                fprintf(fid, '</Points>\n');
                fprintf(fid, '<Cells>\n');
                fprintf(fid, '<DataArray type="Int32" Name="connectivity" NumberOfComponents="1" format="ascii">\n');
                fprintf(fid, '%u %u %u %u ',obj.maxw.mesh.t.'-1);
                fprintf(fid, '\n</DataArray>\n');
                fprintf(fid, '<DataArray type="Int32" Name="offsets" NumberOfComponents="1" format="ascii">\n');
                fprintf(fid, '%d ',4*[1:obj.maxw.mesh.nt]);
                fprintf(fid, '\n</DataArray>\n');
                fprintf(fid, '<DataArray type="UInt8" Name="types" NumberOfComponents="1" format="ascii">\n');
                a=10; % type of element=tetrahedra
                fprintf(fid, '%u ',a(ones(1,obj.maxw.mesh.nt)));
                fprintf(fid, '\n</DataArray>\n');
                fprintf(fid, '</Cells>\n');
                fprintf(fid, '</Piece>\n');
                fprintf(fid, '</UnstructuredGrid>\n');
                fprintf(fid, '</VTKFile>');
                fclose(fid);
            end
        end
        
        function [Bx,By,Bz] = solBNode(obj,filename,varargin)            
            % Projection of solution onto nodes
            space = NGrad(obj.quad);
            ndofs = obj.maxw.mesh.np; % scalar global ndof
            ndofv = obj.maxw.mesh.ne; % vector global ndof
            lndofs = space.ndof; % local scalar dof
            lndofv = obj.maxw.space.ndof; % local vector dof
            nt = obj.maxw.mesh.nt;
            theta_P = zeros(lndofs,lndofs);
            theta_Q = zeros(3,lndofs,lndofv);
            ii = zeros(nt,lndofs*lndofs);
            ji = ii;
            Sv = ii;
            iMi = zeros(nt,lndofs*lndofv);
            jMi = zeros(nt,lndofs*lndofv);
            Mv = zeros(3,nt,lndofs*lndofv);
           
            for i=1:lndofs
                for j=1:lndofs
                   theta_P(i,j) = (space.N{i}.*space.N{j})*obj.quad.W;           
                end
            end
            
            for i=1:lndofs
                for j=1:lndofv
                    for k=1:3   
                        theta_Q(k,i,j) = (space.N{i}.*obj.maxw.space.curlN{j}(k,:))*obj.quad.W;
                    end
                end
            end
                    
            for i=1:lndofs  % local degrees of freedom on ref. elem
                for j=1:lndofs % local degrees of freedom on ref. elem
                    Sv(:,(i-1)*lndofs+j) = Sv(:,(i-1)*lndofs+j) + ...
                        abs(obj.maxw.mesh.detB)* theta_P(i,j);
                    % indexing
                    ii(:,(i-1)*lndofs+j) = obj.maxw.mesh.t(:,i);
                    ji(:,(i-1)*lndofs+j) = obj.maxw.mesh.t(:,j);             
                end
            end
            S = sparse(ii,ji,Sv,ndofs,ndofs) ;
            
            for i=1:lndofs  % local degrees of freedom on ref. elem
                for j=1:lndofv % local degrees of freedom on ref. elem
                    for r=1:3
                        fact = (bsxfun(@rdivide,obj.maxw.mesh.B{r},obj.maxw.mesh.detB));
                        for k=1:3                        
                            Mv(r,:,(i-1)*lndofv+j) = Mv(r,:,(i-1)*lndofv+j) + ...
                               ((fact(:,k).*abs(obj.maxw.mesh.detB))*theta_Q(k,i,j)).';
                        end
                    end
                    % indexing
                    iMi(:,(i-1)*lndofv+j) = obj.maxw.mesh.t(:,i);
                    jMi(:,(i-1)*lndofv+j) = obj.maxw.mesh.el2ed(:,j);
                end
            end
            if nargin>2
                Bx = varargin{1};
                By = varargin{2};
                Bz = varargin{3};
            else
            Mx = sparse(iMi,jMi,squeeze(Mv(1,:,:)),ndofs,ndofv);
            My = sparse(iMi,jMi,squeeze(Mv(2,:,:)),ndofs,ndofv);
            Mz = sparse(iMi,jMi,squeeze(Mv(3,:,:)),ndofs,ndofv);
            Bx = S\(Mx*obj.sol);
            By = S\(My*obj.sol);
            Bz = S\(Mz*obj.sol);
            end
            
            clear Sv Mv ii ji Mx My Mz iMi jMi
            fid = fopen(strcat(filename,'.msh.vtu'), 'w');
            fprintf(fid, '<?xml version="1.0"?>\n');
            fprintf(fid, '<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">\n');
            fprintf(fid, '<UnstructuredGrid>\n');
            fprintf(fid, '<Piece NumberOfPoints="%u" NumberOfCells="%u">\n',obj.maxw.mesh.np,obj.maxw.mesh.nt);
            fprintf(fid, '<PointData>\n');
            fprintf(fid, '<DataArray type="Float64" Name="ElectricFieldReal" NumberOfComponents="3" format="ascii">\n');
            fprintf(fid, '%6.15e %6.15e %6.15e ',[real(Bx(:)),real(By(:)),real(Bz(:))]');
            fprintf(fid, '\n</DataArray>\n');
            fprintf(fid, '<DataArray type="Float64" Name="ElectricFieldIm" NumberOfComponents="3" format="ascii">\n');
            fprintf(fid, '%6.15e %6.15e %6.15e ',[imag(Bx(:)),imag(By(:)),imag(Bz(:))]');
            fprintf(fid, '\n</DataArray>\n');
            fprintf(fid, '</PointData>\n');
            fprintf(fid, '<Points>\n');
            fprintf(fid, '<DataArray type="Float64" Name="position" NumberOfComponents="3" format="ascii">\n');
            fprintf(fid, '%6.12f %6.12f %6.12f ',obj.maxw.mesh.p.');
            fprintf(fid, '\n</DataArray>\n');
            fprintf(fid, '</Points>\n');
            fprintf(fid, '<Cells>\n');
            fprintf(fid, '<DataArray type="Int32" Name="connectivity" NumberOfComponents="1" format="ascii">\n');
            fprintf(fid, '%u %u %u %u ',obj.maxw.mesh.t.'-1);
            fprintf(fid, '\n</DataArray>\n');
            fprintf(fid, '<DataArray type="Int32" Name="offsets" NumberOfComponents="1" format="ascii">\n');
            fprintf(fid, '%d ',4*[1:obj.maxw.mesh.nt]);
            fprintf(fid, '\n</DataArray>\n');
            fprintf(fid, '<DataArray type="UInt8" Name="types" NumberOfComponents="1" format="ascii">\n');
            a=10; % type of element=tetrahedra
            fprintf(fid, '%u ',a(ones(1,obj.maxw.mesh.nt)));
            fprintf(fid, '\n</DataArray>\n');
            fprintf(fid, '</Cells>\n');
            fprintf(fid, '</Piece>\n');
            fprintf(fid, '</UnstructuredGrid>\n');
            fprintf(fid, '</VTKFile>');            
        end     
        
        function exportInterpSol(obj,filename,varargin)
            % Save mesh and electric field to VTK XML format
            if nargin < 5
                [Ex,Ey,Ez,psub] = obj.solInterp; 
            else
                Ex = varargin{1};
                Ey = varargin{2};
                Ez = varargin{3};
                psub = varargin{4};
            end
            %% Solution at quadrature points
            fid = fopen(strcat(filename,'.sol.vtu'), 'w');
            fprintf(fid, '<?xml version="1.0"?>\n');
            fprintf(fid, '<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">\n');
            fprintf(fid, '<UnstructuredGrid>\n');
            fprintf(fid, '<Piece NumberOfPoints="%u" NumberOfCells="%u">\n',size(psub,1),size(psub,1));
            fprintf(fid, '<Points>\n');
            fprintf(fid, '<DataArray type="Float64" Name="position" NumberOfComponents="3" format="ascii">\n');
            fprintf(fid, '%6.12f %6.12f %6.12f ',psub');
            fprintf(fid, '\n</DataArray>\n');
            fprintf(fid, '</Points>\n');
            fprintf(fid, '<PointData>\n');
            fprintf(fid, '<DataArray type="Float64" Name="ElectricFieldReal" NumberOfComponents="3" format="ascii">\n');
            fprintf(fid, '%6.15e %6.15e %6.15e ',[real(Ex(:)),real(Ey(:)),real(Ez(:))]');
            fprintf(fid, '\n</DataArray>\n');
            fprintf(fid, '<DataArray type="Float64" Name="ElectricFieldIm" NumberOfComponents="3" format="ascii">\n');
            fprintf(fid, '%6.15e %6.15e %6.15e ',[imag(Ex(:)),imag(Ey(:)),imag(Ez(:))]');
            fprintf(fid, '\n</DataArray>\n');
            fprintf(fid, '</PointData>\n');
            fprintf(fid, '<Cells>\n');
            fprintf(fid, '<DataArray type="Int32" Name="connectivity" NumberOfComponents="1" format="ascii">\n');
            fprintf(fid, '%d ',0:size(psub,1)-1);
            fprintf(fid, '\n</DataArray>\n');
            fprintf(fid, '<DataArray type="Int32" Name="offsets" NumberOfComponents="1" format="ascii">\n');
            fprintf(fid, '%d ',1:size(psub,1));
            fprintf(fid, '\n</DataArray>\n');
            fprintf(fid, '<DataArray type="UInt8" Name="types" NumberOfComponents="1" format="ascii">\n');
            a=1; % type of element = vertex
            fprintf(fid, '%u ',a(ones(1,size(psub,1))));
            fprintf(fid, '\n</DataArray>\n');            
            fprintf(fid, '</Cells>\n');
            fprintf(fid, '</Piece>\n');
            fprintf(fid, '</UnstructuredGrid>\n');
            fprintf(fid, '</VTKFile>');
            fclose(fid);                    
        end
            
        function [V,b,c,d] = calcHatGradients(obj)
            % Calculate area and gradients for hat basis functions
            % in 3D tetrahedral mesh
            % Output: Volume of tetrahedron and gradient=[b,c,d]'
            V = abs(obj.maxw.mesh.detB)/6;
            % Gradients in reference tetrahedron
            gradPhi(:,1) = [-1;-1;-1];
            gradPhi(:,2) = [1;0;0];
            gradPhi(:,3) = [0;1;0];
            gradPhi(:,4) = [0;0;1];
            b = zeros(obj.maxw.mesh.nt,4);
            c = b;
            d = b;
            % Mapping
            for i=1:4
                b(:,i) = obj.maxw.mesh.invBT{1}*gradPhi(:,i);
                c(:,i) = obj.maxw.mesh.invBT{2}*gradPhi(:,i);
                d(:,i) = obj.maxw.mesh.invBT{3}*gradPhi(:,i);
            end        
        end 
        
        function [L2,L2rel] = L2err(obj, exact)
            % Calculate L2 error
            l = obj.maxw.mesh.nt*obj.quad.nint;
            x = zeros(obj.quad.nint,obj.maxw.mesh.nt,3);
            % Global integration points
            for i=1:3
                x(:,:,i) = bsxfun(@plus, obj.maxw.mesh.B{i}*obj.quad.X, obj.maxw.mesh.b{i}).';
            end
            exE = exact(reshape(x,l,3));
            exEx = reshape(exE(:,1),obj.quad.nint,obj.maxw.mesh.nt);
            exEy = reshape(exE(:,2),obj.quad.nint,obj.maxw.mesh.nt);
            exEz = reshape(exE(:,3),obj.quad.nint,obj.maxw.mesh.nt);
            [Ex,Ey,Ez,~] = obj.solInterp();     
            L2 = sum(bsxfun(@times,(exEx-Ex).*conj(exEx-Ex) + (exEy-Ey).*conj(exEy-Ey) + ...
                (exEz-Ez).*conj(exEz-Ez),obj.quad.W),1);
            L2true = sum(bsxfun(@times,(exEx).*conj(exEx) + (exEy).*conj(exEy) + ...
                (exEz).*conj(exEz),obj.quad.W),1);
            L2 = L2.*abs(obj.maxw.mesh.detB).';
            L2true = L2true.*abs(obj.maxw.mesh.detB).';            
            L2 = sqrt(sum(L2));      
            L2true = sqrt(sum(L2true));
            L2rel = L2/L2true;
        end
        
        function [Hx,Hy,Hz] = calcH(obj)
            % Calculate the magnetic field            
            % Solution values            
            sol1 = obj.sol(obj.maxw.mesh.el2ed(:,1));
            sol2 = obj.sol(obj.maxw.mesh.el2ed(:,2));
            sol3 = obj.sol(obj.maxw.mesh.el2ed(:,3));
            sol4 = obj.sol(obj.maxw.mesh.el2ed(:,4));
            sol5 = obj.sol(obj.maxw.mesh.el2ed(:,5));
            sol6 = obj.sol(obj.maxw.mesh.el2ed(:,6));
            curl = cell(1,6);
            % map from ref to global
            for n=1:6   
                curl{n} = zeros(obj.maxw.mesh.nt,obj.quad.nint,3);
                for i = 1:3
                    curl{n}(:,:,i) = (bsxfun(@rdivide,obj.maxw.mesh.B{i},obj.maxw.mesh.detB))*obj.maxw.space.curlN{n};
                end
            end
            
            % linear combination
            Hx = bsxfun(@times,sol1,curl{1}(:,:,1)) + bsxfun(@times,sol2,curl{2}(:,:,1)) + ...
                 bsxfun(@times,sol3,curl{3}(:,:,1)) + bsxfun(@times,sol4,curl{4}(:,:,1)) + ...
                 bsxfun(@times,sol5,curl{5}(:,:,1)) + bsxfun(@times,sol6,curl{6}(:,:,1));
            
            Hy = bsxfun(@times,sol1,curl{1}(:,:,2)) + bsxfun(@times,sol2,curl{2}(:,:,2)) + ...
                 bsxfun(@times,sol3,curl{3}(:,:,2)) + bsxfun(@times,sol4,curl{4}(:,:,2)) + ...
                 bsxfun(@times,sol5,curl{5}(:,:,2)) + bsxfun(@times,sol6,curl{6}(:,:,2));
            
            Hz = bsxfun(@times,sol1,curl{1}(:,:,3)) + bsxfun(@times,sol2,curl{2}(:,:,3)) + ...
                 bsxfun(@times,sol3,curl{3}(:,:,3)) + bsxfun(@times,sol4,curl{4}(:,:,3)) + ...
                 bsxfun(@times,sol5,curl{5}(:,:,3)) + bsxfun(@times,sol6,curl{6}(:,:,3));   
            
            Hx = (bsxfun(@times,obj.maxw.pde.invmu,Hx)./(1j*obj.maxw.pde.omega)).';
            Hy = (bsxfun(@times,obj.maxw.pde.invmu,Hy)./(1j*obj.maxw.pde.omega)).';
            Hz = (bsxfun(@times,obj.maxw.pde.invmu,Hz)./(1j*obj.maxw.pde.omega)).';
        end
        
        function [Sx,Sy,Sz] = calcS(obj)
            % Calculate Poynting vector P=0.5*E X conj(H)
            [Hx,Hy,Hz] = obj.calcH();
            Hx = conj(Hx);
            Hy = conj(Hy);
            Hz = conj(Hz);
            [Ex,Ey,Ez,~] = obj.solInterp();
            Sx = 0.5*(Ey.*Hz - Ez.*Hy);
            Sy = -0.5*(Ex.*Hz - Ez.*Hx);
            Sz = 0.5*(Ex.*Hy - Ey.*Hx);
        end
        
        function Hcurl = Hcurlerr(obj, exact, curlexact)
            % Calculate Hcurl error
            l = obj.maxw.mesh.nt*obj.quad.nint;
            x = zeros(obj.quad.nint,obj.maxw.mesh.nt,3);
            [L2,~] = obj.L2err(exact);
            % Global integration points
            for i=1:3
                x(:,:,i) = bsxfun(@plus, obj.maxw.mesh.B{i}*obj.quad.X, obj.maxw.mesh.b{i}).';
            end
            exH = curlexact(reshape(x,l,3));
            exHx = reshape(exH(:,1),obj.quad.nint,obj.maxw.mesh.nt);
            exHy = reshape(exH(:,2),obj.quad.nint,obj.maxw.mesh.nt);
            exHz = reshape(exH(:,3),obj.quad.nint,obj.maxw.mesh.nt);
            [Hx,Hy,Hz] = obj.calcH();
            ce = sum(bsxfun(@times,(exHx-Hx).*conj(exHx-Hx) + (exHy-Hy).*conj(exHy-Hy) + ...
                (exHz-Hz).*conj(exHz-Hz),obj.quad.W),1);
            ce = ce.*abs(obj.maxw.mesh.detB).';
            ce = sum(ce);   
            Hcurl = sqrt(L2^2 + ce);           
        end
        
        function l2 = dl2err(obj)
            % Calculate discrete l2 error
            e = obj.maxw.edInterp(exact,obj.maxw.mesh.p,obj.maxw.mesh.edges);
            l2 = sqrt(sum((e-obj.sol).*conj(e-obj.sol)));
        end 
    end
end
