classdef MaxwPDE < handle
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % PDE class for Maxwell's time-harmonic electric wave eq.
    % PDE parameters are defined here
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Copyright Dimitris Kamilis, 2017
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    properties(SetAccess = private)
        invmu % 1/mu
        fhat % J_ext
        omega % Angular frequency
        kappa %
        gD % Dirichlet boundary condition
        gN % Neumann boundary condition
        sourceType % Incident, finite dipole or point dipole
        sourceDOF % Needed for finite dipole defined on an edge
        sourcePos %
        deltaSigma % For secondary field approach
        dimless % True for dimensionless form
        dimscale % Length scale L
        refcurr % Reference electric current I0c
        verbose
        constants
    end

    methods
        function obj = MaxwPDE(msh, varargin)
            p = inputParser;
            p.FunctionName = 'MaxwPDE';
            p.PartialMatching = true;
            obj.constants.epsilon0 = 8.854187817*10^(-12);
            obj.constants.mu0 = 4*pi*10^(-7);
            obj.constants.omega0 = 2*pi;
            obj.constants.sigma0 = 0;
            obj.constants.c0 = 3*10^8;
            obj.constants.Z0 = sqrt(obj.constants.mu0/obj.constants.epsilon0);
            addRequired(p, 'msh',@(x) isa(x,'Tetmesh'));
            addParameter(p,'sigma',obj.constants.sigma0);
            addParameter(p,'epsilon',obj.constants.epsilon0);
            addParameter(p,'mu',obj.constants.mu0);
            addParameter(p,'omega',obj.constants.omega0);
            addParameter(p,'gD',0);
            addParameter(p,'gN',0);
            addParameter(p,'sourceType','incident');
            addParameter(p,'sourcePos',[]);
            addParameter(p,'J',0);
            addParameter(p,'deltaSigma',[])
            addParameter(p,'sourceDOF',[])
            addParameter(p,'dimless',false);
            addParameter(p,'dimscale',1);
            addParameter(p,'refcurr',1);
            addParameter(p,'verbose',false);
            try
                parse(p, msh, varargin{:})
            catch exception
                error('Wrong input parameters.')
            end
            obj.omega = p.Results.omega;
            mu = p.Results.mu;
            epsilon = p.Results.epsilon;
            sigma = p.Results.sigma;
            J = p.Results.J;
            obj.deltaSigma = p.Results.deltaSigma;
            obj.gD = p.Results.gD;
            obj.gN = p.Results.gN;
            obj.sourceType = p.Results.sourceType;
            obj.sourcePos = p.Results.sourcePos;
            obj.sourceDOF = p.Results.sourceDOF;
            obj.dimless = p.Results.dimless;
            obj.dimscale = p.Results.dimscale;
            obj.refcurr = p.Results.refcurr;
            obj.verbose = p.Results.verbose;
            if obj.verbose==1
                for i=1:length(p.UsingDefaults)
                    param = p.UsingDefaults{i};
                    value = p.Results.(param);
                    if isnumeric(value) || isfloat(value) || isinteger(value)
                        fprintf('Using default value for %s=%G\n',param,value)
                    else
                        fprintf('Using default value for %s=%s\n',param,value)
                    end
                end
            end
            center = p.Results.msh.calcCenter;
            if obj.dimless == false
                if isa(mu,'function_handle')
                    obj.invmu = 1./mu(center);
                else
                    obj.invmu = 1./mu(ones(p.Results.msh.nt,1));
                end
                if isa(sigma,'function_handle')
                    try
                        sigma(1,1,1);
                    catch err
                        error('Parameters must be handles with (x,y,z) input');
                    end
                    obj.kappa = -obj.omega*(1i*sigma(center(:,1),center(:,2),center(:,3)) + ...
                        obj.omega*epsilon*ones(msh.nt,1));
                elseif iscolumn(sigma)
                    obj.kappa = -obj.omega*(1i*sigma + ...
                        obj.omega*epsilon*ones(length(sigma),1));
                elseif isscalar(sigma)
                    sigma = sigma*ones(msh.nt,1);
                    obj.kappa = -obj.omega*(1i*sigma(:) + ...
                        obj.omega*epsilon*ones(msh.nt,1));
                elseif ismatrix(sigma)
                    obj.kappa =  -obj.omega*(1i*sigma + obj.omega*repmat(epsilon,size(sigma)));
                else
                    error('Parameters not in correct input format')
                end
               % assert(isequal(size(obj.kappa),[1,msh.nt]) || isequal(size(obj.kappa),[1]))
                if strcmp(obj.sourceType,'incident')
                    if isa(J,'function_handle')
                        obj.fhat = J;
                    else
                        error('Source input not in correct format')
                    end
                    if  isa(obj.deltaSigma,'function_handle')
                        obj.deltaSigma = obj.deltaSigma(center(:,1),center(:,2),center(:,3));
                    elseif isscalar(obj.deltaSigma)
                        obj.deltaSigma = obj.deltaSigma(ones(p.Results.msh.nt,1));
                    elseif iscolumn(obj.deltaSigma)                      
                        obj.deltaSigma = obj.deltaSigma;
                    elseif ismatrix(obj.deltaSigma)
                        obj.deltaSigma = obj.deltaSigma;
                    end
                elseif strcmp(obj.sourceType,'edipolefinite')
                    if isempty(obj.sourceDOF)
                        error('Source DOF needed')
                    end
                    obj.fhat = J;
                elseif strcmp(obj.sourceType,'edipole')
                    obj.fhat = J;   
                elseif strcmp(obj.sourceType,'solution')
                    obj.fhat = J;  
                elseif strcmp(obj.sourceType,'edipoleReg')
                    obj.fhat = J;
                elseif strcmp(obj.sourceType,'vector')
                    if isscalar(J)
                        obj.fhat = J(ones(p.Results.msh.nt,3));
                    elseif isvector(J)
                        obj.fhat = J;
                    end
                    if  isa(obj.deltaSigma,'function_handle')
                        obj.deltaSigma = obj.deltaSigma(center(:,1),center(:,2),center(:,3));
                    elseif isscalar(obj.deltaSigma)
                        obj.deltaSigma = obj.deltaSigma(ones(p.Results.msh.nt,1));
                    elseif isvector(obj.deltaSigma)                      
                        obj.deltaSigma = obj.deltaSigma(:);
                    end
                else
                    error('Invalid source type')
                end
            elseif obj.dimless == true % Dimensionless form
                 if isa(mu,'function_handle')
                    obj.invmu = (obj.constants.mu0./mu(center))*obj.dimscale^2;
                else
                    obj.invmu = obj.constants.mu0./mu(ones(p.Results.msh.nt,1))*obj.dimscale^2;
                end
                obj.omega = obj.omega*obj.dimscale/obj.constants.c0;
                epsilon = epsilon/obj.constants.epsilon0;
                if isa(sigma,'function_handle')
                    try
                        sigma(1,1,1);
                    catch err
                        error('Parameters must be handles with (x,y,z) input');
                    end
                    obj.kappa = -obj.omega*(1i*obj.constants.Z0*obj.dimscale*sigma(center(:,1),center(:,2),center(:,3)) + ...
                        obj.omega*epsilon*ones(msh.nt,1));
                elseif iscolumn(sigma)
                    obj.kappa = -obj.omega*(1i*obj.constants.Z0*obj.dimscale*sigma + ...
                        obj.omega*epsilon*ones(length(sigma),1));
                elseif isscalar(sigma)
                    sigma = sigma*ones(msh.nt,1);
                    obj.kappa = -obj.omega*(1i*obj.constants.Z0*obj.dimscale*sigma(:) + ...
                        obj.omega*epsilon*ones(msh.nt,1));
                elseif ismatrix(sigma)
                    obj.kappa =  -obj.omega*(1i*obj.constants.Z0*obj.dimscale*sigma + obj.omega*repmat(epsilon,size(sigma)));
                else
                    error('Parameters not in correct input format')
                end
               % assert(isequal(size(obj.kappa),[1,msh.nt]) || isequal(size(obj.kappa),[1]))
                if strcmp(obj.sourceType,'incident')
                    if isa(J,'function_handle')
                         obj.fhat = @(varargin) (obj.dimscale/obj.refcurr).*J(varargin{:})/obj.constants.Z0;
                    else
                        error('Source input not in correct format')
                    end
                    if  isa(obj.deltaSigma,'function_handle')
                        obj.deltaSigma = obj.constants.Z0*obj.dimscale*obj.deltaSigma(center(:,1),center(:,2),center(:,3));
                    elseif isscalar(obj.deltaSigma)
                        obj.deltaSigma = obj.constants.Z0*obj.dimscale*obj.deltaSigma(ones(p.Results.msh.nt,1));
                    elseif iscolumn(obj.deltaSigma)      
                        obj.deltaSigma = obj.constants.Z0*obj.dimscale*obj.deltaSigma;
                    elseif ismatrix(obj.deltaSigma)
                        obj.deltaSigma = obj.constants.Z0*obj.dimscale*obj.deltaSigma;
                    end
                elseif strcmp(obj.sourceType,'edipolefinite')
                    if isempty(obj.sourceDOF)
                        error('Source DOF needed')
                    end
                    obj.fhat = obj.dimscale^2/obj.refcurr*J;
                elseif strcmp(obj.sourceType,'edipole')
                    obj.fhat = obj.dimscale^2/obj.refcurr*J;
                elseif strcmp(obj.sourceType,'vector')
                    if isscalar(J)
                        obj.fhat = obj.dimscale^2./obj.refcurr*J(ones(p.Results.msh.nt,3));
                    elseif isvector(J)
                        obj.fhat = obj.dimscale^2./obj.refcurr*J;
                    end
                else
                    error('Invalid source type')
                end          
            end
        end % constructor

        function E = dimless2dim(obj, Es)
            E = (obj.refcurr*obj.constants.Z0/obj.dimscale).*Es;
        end
    end % methods
end
