classdef Quadrature
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Quadrature class for 1,2,3 dimensions
    % 1D: [0,1] line segment (2,3-point formulas)
    % 2D: Reference triangle (1,3,7-point formulas)
    % 3D: Referece tetrahedron (1,4,10,20,35,56-point formulas)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Copyright Dimitris Kamilis, 2017
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    properties
        nint
        order
        X
        W
        dim
    end
    
    methods
        function obj = Quadrature(dim,order)
            % Constructor
                if dim==1
                    switch order 
                        case 2 
                            obj.X = [0 1];
                            obj.W = [1;1]/2;
                        case 3
                            obj.X = [0 1/2 1];
                            obj.W = [1;4;1]/6;
                    end
                elseif dim==2
                    switch order   
                        case 1
                            obj.X = [1/3;1/3];
                            obj.W = 1/2;
                        case 2
                            obj.X = [1/6 2/3 1/6;1/6 1/6 2/3]; 
                            obj.W = [1/6;1/6;1/6];
                        case 3
                            obj.X = [1/3 1/2 1/2 0 1 0 0;1/3 0 1/2 1/2 0 1 0];
                            obj.W = [9/40;1/15;1/15;1/15;1/40;1/40;1/40];
                    end
                elseif dim==3
                    rules = load('QuadRulesTet.mat');
                    Quad = rules.Quad;
                    % Rules taken from "Symmetric quadrature rules for tetrahedra based on a cubic
                    % close-packed lattice arrangement"
                    switch order
                        case 1
                            obj.X = (Quad{1}(2:4)).';
                            obj.W = (Quad{1}(6))/6;
                        case 2
                            obj.X = (Quad{2}(:,2:4)).';
                            obj.W = (Quad{2}(:,6))/6;
                        case 3
                           obj.X = (Quad{3}(:,2:4)).';
                           obj.W = (Quad{3}(:,6))/6;
                        case 4
                           obj.X = (Quad{4}(:,2:4)).';
                           obj.W = (Quad{4}(:,6))/6;
                        case 5
                           obj.X = (Quad{5}(:,2:4)).';
                           obj.W = (Quad{5}(:,6))/6;
                        case 6                           
                           obj.X = (Quad{6}(:,2:4)).';
                           obj.W = (Quad{6}(:,6))/6;
                    end
                else
                    error ('Only dimensions 1,2,3 supported')
                end           
            obj.order = order;
            obj.nint = length(obj.W);
            obj.dim = dim;         
        end
    end
end