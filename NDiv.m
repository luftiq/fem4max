classdef NDiv < Element
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Class for lowest order Raviart-Thomas face elements
    % The basis functions and their div are defined on the
    % reference tetrahedron
    % Properties: N{i} where i=1,...,4 containt the face lowest order 
    %             Raviart-Thomas basis functions for the reference tetrahedron 
    %             evaluated at the quadrature points defined by the input object
    %             'tetquad'
    %             divN{i} where i=1,...,4 contains the corresponding div
    %             tetquad - an instance of the quadrature class for 3D 
    %             tetrahedra. Contains the integration points and weights
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Copyright Dimitris Kamilis, 2017
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    properties (SetAccess = private)
        N
        divN
        tetquad
        order = 1;
        ndof = 4; % number of degrees of freedom
        type = 'NDiv';
    end
    
    methods
        function obj = NDiv(tetquad)
            if tetquad.dim~=3 
                error('Quadrature rule for tetraheda needed')
            end
            obj.tetquad = tetquad;
            X = tetquad.X;            
            obj.N = cell(obj.ndof,1);
            obj.divN = cell(obj.ndof,1);
            obj.N{1} = 2*[X(1,:) ; X(2,:) ; X(3,:)-1];
            obj.N{2} = 2*[X(1,:) ; X(2,:)-1; X(3,:)];
            obj.N{3} = 2*[X(1,:)-1 ; X(2,:) ; X(3,:)];
            obj.N{4} = 2*[X(1,:) ; X(2,:) ; X(3,:)];
            obj.divN{1} = repmat(6,1,tetquad.nint);
            obj.divN{2} = repmat(6,1,tetquad.nint);
            obj.divN{3} = repmat(6,1,tetquad.nint);
            obj.divN{4} = repmat(6,1,tetquad.nint);
        end         
    end   
end