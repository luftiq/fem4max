classdef FemMat
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Class for assembly of unit coefficient FEM matrices
    % for the curl, div and grad conforming lowest order spaces
    % It also assembles matrices for div-DG mixed
    % formulations
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Copyright Dimitris Kamilis, 2017
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties
        mesh
        space
    end
    
    methods
        function obj = FemMat(mesh,space)
            obj.mesh = mesh;
            obj.space = space;
        end
        
        function M = assembleGram(obj)
            %ok
            if strcmp(obj.space.type,'NGrad')
                ndof = obj.mesh.np; % scalar global ndof
                lndof = obj.space.ndof; % local scalar dof
                nt = obj.mesh.nt;
                ii = zeros(nt,lndof*lndof);
                ji = ii;
                Mv = ii;
                for i=1:lndof  % local degrees of freedom on ref. elem
                    for j=1:lndof % local degrees of freedom on ref. elem
                        Mv(:,(i-1)*lndof+j) = Mv(:,(i-1)*lndof+j) + ...
                            (obj.space.N{i}.*obj.space.N{j})*obj.space.tetquad.W.*abs(obj.mesh.detB);
                        % indexing
                        ii(:,(i-1)*lndof+j) = obj.mesh.t(:,i);
                        ji(:,(i-1)*lndof+j) = obj.mesh.t(:,j);
                    end
                end
                M = sparse(ii,ji,Mv,ndof,ndof);
                M = 0.5*(M+M.');
            elseif strcmp(obj.space.type,'NCurl')
                %ok
                ndof = obj.mesh.ne; % global dof
                lndof = obj.space.ndof; % local dof
                nt = obj.mesh.nt;
                ii = zeros(nt,lndof*lndof);
                ji = ii;
                Mv = ii;
                Ndx = cell(lndof,3);               
                for n=1:lndof
                    for i = 1:3
                        Ndx{n,i} = obj.mesh.invBT{i}*obj.space.N{n};                      
                    end
                end               
                for n=1:lndof
                    for m=1:lndof
                        for i = 1:3
                                Mv(:,(n-1)*lndof+m) = Mv(:,(n-1)*lndof+m) + ...
                                    ((Ndx{n,i}.*Ndx{m,i})*obj.space.tetquad.W.*abs(obj.mesh.detB));
                        end
                        ii(:,(n-1)*lndof+m) = obj.mesh.el2ed(:,n);
                        ji(:,(n-1)*lndof+m) = obj.mesh.el2ed(:,m);
                    end
                end
                M = sparse(ii,ji,Mv,ndof,ndof);
                M = 0.5*(M+M.');
            elseif strcmp(obj.space.type,'NDiv')
                %ok
                ndof = obj.mesh.nf; % global dof
                lndof = obj.space.ndof; % local dof
                nt = obj.mesh.nt;
                ii = zeros(nt,lndof*lndof);
                ji = ii;
                Mv = ii;
                Ndx = cell(lndof,3);
                for n=1:lndof
                    for i = 1:3
                        Ndx{n,i} = (bsxfun(@rdivide,obj.mesh.B{i},obj.mesh.detB))*obj.space.N{n};                      
                    end
                end 
                for n=1:lndof
                    for m=1:lndof
                        for i = 1:3
                                Mv(:,(n-1)*lndof+m) = Mv(:,(n-1)*lndof+m) + ...
                                    ((Ndx{n,i}.*Ndx{m,i})*obj.space.tetquad.W.*abs(obj.mesh.detB));
                        end
                        ii(:,(n-1)*lndof+m) = obj.mesh.el2fa(:,n);
                        ji(:,(n-1)*lndof+m) = obj.mesh.el2fa(:,m);
                    end
                end
                M = sparse(ii,ji,Mv,ndof,ndof);
                M = 0.5*(M+M.');
            else
                error('Space type not valid')
            end
        end
        
        function M = assembleStiff(obj)
            if strcmp(obj.space.type,'NGrad')
                ndof = obj.mesh.np; % global dof
                lndof = obj.space.ndof; % local dof
                nt = obj.mesh.nt;
                ii = zeros(nt,lndof*lndof);
                ji = ii;
                Mv = ii;
                gradNdx = cell(lndof,3);
                for n=1:lndof
                    for i = 1:3
                        gradNdx{n,i} = obj.mesh.invBT{i}*obj.space.gradN{n};                     
                    end
                end               
                for n=1:lndof
                    for m=1:lndof
                        for i = 1:3
                                Mv(:,(n-1)*lndof+m) = Mv(:,(n-1)*lndof+m) + ...
                                    ((gradNdx{n,i}.*gradNdx{m,i})*obj.space.tetquad.W.*abs(obj.mesh.detB));
                        end
                        ii(:,(n-1)*lndof+m) = obj.mesh.t(:,n);
                        ji(:,(n-1)*lndof+m) = obj.mesh.t(:,m);
                    end
                end
                M = sparse(ii,ji,Mv,ndof,ndof);
                M = 0.5*(M+M.');
            elseif strcmp(obj.space.type,'NCurl')
                ndof = obj.mesh.ne; % global dof
                lndof = obj.space.ndof; % local dof
                nt = obj.mesh.nt;
                ii = zeros(nt,lndof*lndof);
                ji = ii;
                Mv = ii;               
                curlNdx = cell(lndof,3);
                for n=1:lndof
                    for i = 1:3                       
                        curlNdx{n,i} = (bsxfun(@rdivide,obj.mesh.B{i},obj.mesh.detB))*obj.space.curlN{n};
                    end
                end               
                for n=1:lndof
                    for m=1:lndof
                        for i = 1:3
                                Mv(:,(n-1)*lndof+m) = Mv(:,(n-1)*lndof+m) + ...
                                    ((curlNdx{n,i}.*curlNdx{m,i})*obj.space.tetquad.W.*abs(obj.mesh.detB));
                        end
                        ii(:,(n-1)*lndof+m) = obj.mesh.el2ed(:,n);
                        ji(:,(n-1)*lndof+m) = obj.mesh.el2ed(:,m);
                    end
                end
                M = sparse(ii,ji,Mv,ndof,ndof);
                M = 0.5*(M+M.');
            elseif strcmp(obj.space.type,'NDiv')
                ndof = obj.mesh.nf; % global dof
                lndof = obj.space.ndof; % local dof
                nt = obj.mesh.nt;
                ii = zeros(nt,lndof*lndof);
                ji = ii;
                Mv = ii;
                divNdx = cell(lndof,1);
                for n=1:lndof                       
                        divNdx{n} = (bsxfun(@rdivide,1,obj.mesh.detB))*obj.space.divN{n}; 
                end 
                for n=1:lndof
                    for m=1:lndof
                                Mv(:,(n-1)*lndof+m) = Mv(:,(n-1)*lndof+m) + ...
                                    ((divNdx{n}.*divNdx{m})*obj.space.tetquad.W.*abs(obj.mesh.detB));
                        ii(:,(n-1)*lndof+m) = obj.mesh.el2fa(:,n);
                        ji(:,(n-1)*lndof+m) = obj.mesh.el2fa(:,m);
                    end
                end
                M = sparse(ii,ji,Mv,ndof,ndof);
                M = 0.5*(M+M.');
            else
                error('Space type not valid')
            end
        end 
        
        function M = assembleDivDG(obj)           
            ndof = obj.mesh.nf; % global dof
            lndof = obj.space.ndof; % local dof
            nt = obj.mesh.nt;            
            Mv = zeros(nt,lndof);            
            divNdx = cell(lndof,1);
            
            for n=1:lndof
                divNdx{n} = (bsxfun(@rdivide,1,obj.mesh.detB))*obj.space.divN{n};
            end
            for n=1:lndof                                
                Mv(:,n) = Mv(:,n) + ...
                    ((divNdx{n})*obj.space.tetquad.W).*abs(obj.mesh.detB);                               
            end
            
            colind = repmat(1:nt,lndof,1);
            colind = colind(:);
            rowind = double(obj.mesh.el2fa.');
            rowind = rowind(:);           
            Mv = permute(Mv,[2,3,1]);                      
            M = sparse(rowind,colind,Mv(:),ndof,nt);
        end
        
    end %methods 
end