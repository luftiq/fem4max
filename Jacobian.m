classdef Jacobian < handle
    % Toolbox class for Jacobian calculation
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%% Copyright Dimitris Kamilis 2017
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    properties
        mesh
        pde
        space
        pp
        Mv
        MvCalc
    end

    methods
        function obj = Jacobian(mesh,pde,space,postproc)
            obj.mesh = mesh;
            obj.pde = pde;
            obj.space = space;
            obj.pp = postproc;
            obj.MvCalc = true;
        end

        function J = ContJac(obj,sol,adjSol)
            % Calculates Jacobian based on adjoint solution
            l = obj.mesh.nt*obj.pp.quad.nint;
            x = zeros(obj.pp.quad.nint,obj.mesh.nt,3);
            % Global integration points
            for i=1:3
                x(:,:,i) = bsxfun(@plus, obj.mesh.B{i}*obj.pp.quad.X, obj.mesh.b{i}).';
            end
            if isa(adjSol,'function_handle')
                jadj = adjSol(reshape(x,l,3));
            else
                jadj = adjSol;
            end
            if isa(sol,'function_handle')
                jsol = sol(reshape(x,l,3));
            else
                jsol = sol;
            end
            jadjx = reshape(jadj(:,1),obj.space.tetquad.nint,obj.mesh.nt);
            jadjy = reshape(jadj(:,2),obj.space.tetquad.nint,obj.mesh.nt);
            jadjz = reshape(jadj(:,3),obj.space.tetquad.nint,obj.mesh.nt);
            jsolx = reshape(jsol(:,1),obj.space.tetquad.nint,obj.mesh.nt);
            jsoly = reshape(jsol(:,2),obj.space.tetquad.nint,obj.mesh.nt);
            jsolz = reshape(jsol(:,3),obj.space.tetquad.nint,obj.mesh.nt);
            J = sum(bsxfun(@times,(jsolx.*jadjx) + (jsoly.*jadjy) + ...
                (jsolz.*jadjz),obj.pp.quad.W),1);
            J = J.*abs(obj.mesh.detB).';
        end

        function G = calcG(obj,Js,sigma,tm)
            % Calculates matrix G for Jacobian calculation
            % tm is the parameter transformation
            ndof = obj.mesh.ne; % global dof
            lndof = obj.space.ndof; % local dof
            nt = sum(obj.mesh.activeCells);
            fv = zeros(nt,lndof);
            if ~isa(Js,'function_handle')
                if Js==0
                  lfx = 0;
                  lfy = 0;
                  lfz = 0;
                end
            else
            activeMask = repmat(obj.mesh.activeCells.',obj.space.tetquad.nint,1);
            lf = Js(obj.pp.maxw.glbIntx(:,1),obj.pp.maxw.glbIntx(:,2),obj.pp.maxw.glbIntx(:,3));
            lfx = reshape(lf(activeMask(:),1),obj.space.tetquad.nint,nt).';
            lfy = reshape(lf(activeMask(:),2),obj.space.tetquad.nint,nt).';
            lfz = reshape(lf(activeMask(:),3),obj.space.tetquad.nint,nt).';
            end
            for n=1:lndof
                fv(:,n) = (obj.pp.maxw.Ndx{n,1}(obj.mesh.activeCells,:).*lfx + obj.pp.maxw.Ndx{n,2}(obj.mesh.activeCells,:).*lfy + ...
                    obj.pp.maxw.Ndx{n,3}(obj.mesh.activeCells,:).*lfz)*obj.space.tetquad.W.*abs(obj.mesh.detB(obj.mesh.activeCells));
            end
            if obj.MvCalc
                obj.Mv = zeros(nt,lndof,lndof);
                for n=1:lndof
                    for m=1:lndof
                        for i = 1:3
                            obj.Mv(:,n,m) = obj.Mv(:,n,m) + ...
                                (obj.pp.maxw.Ndx{n,i}(obj.mesh.activeCells,:).*obj.pp.maxw.Ndx{m,i}(obj.mesh.activeCells,:))*obj.space.tetquad.W.*abs(obj.mesh.detB(obj.mesh.activeCells));
                        end
                    end
                end
                obj.Mv = permute(obj.Mv,[2,3,1]);
                obj.MvCalc = false;
            end
            colind = repmat(1:nt,lndof,1);
            colind = colind(:);
            rowind = double(obj.mesh.el2ed(obj.mesh.activeCells,:).');
            rowind = rowind(:);
            sol = reshape(obj.pp.sol(obj.mesh.el2ed(obj.mesh.activeCells,:)).',[6,1,nt]);
            Gloc = 1i*obj.pde.omega*(fv.'+ squeeze(mtimesx(obj.Mv,sol)));
            if tm(exp(1))==1 %  check of log(m) transformation
                Gloc = bsxfun(@times,Gloc,sigma(obj.mesh.activeCells).');
            elseif (4*tm(2) + tm(3)) == 11 % Stupid check for identity transformation
                 % do nothing
            else
                error('Model transformation not supported')
            end
            G = sparse(rowind,colind,Gloc(:),ndof,nt);
            G = G(obj.pp.maxw.free,:);

        end
        
        function J = DiscrJac(obj,Q,Ep,sigma,map,method,varargin)
            % Calculates discrete Jacobian based on J = P A^(-1) Q
            global AA
            if length(varargin)==3
                L = varargin{1};
                U = varargin{2};
                perm = varargin{3};
            end
            G = obj.calcG(Ep,sigma,map);
            if strcmp(method,'direct')
                xiAdj = AA\Q.';
            else                
                nTerms = size(Q,1);
                nn = size(Q,2);
                xiAdj = zeros(nn,nTerms);
                % how many iterations of the for loop do we expect?
                N = nTerms;
                %   % how often should the monitor update it's progress?
                percentage_update = 0.1;
                %   % show debugging information.
                   do_debug = 1;
                %   % by default we always run 'javaaddpath'. Set this to 0 if you are
                %   % making use of globals.
                run_javaaddpath = 0;
                %   % by default we show the execution time once ParforProgress ends.
                show_execution_time = 1;
                try % Initialization
                    ppm = ParforProgressStarter2('jac', N, percentage_update, do_debug, run_javaaddpath, show_execution_time);
                catch me % make sure "ParforProgressStarter2" didn't get moved to a different directory
                    if strcmp(me.message, 'Undefined function or method ''ParforProgressStarter2'' for input arguments of type ''char''.')
                        error('ParforProgressStarter2 not in path.');
                    else
                        %           % this should NEVER EVER happen.
                        msg{1} = 'Unknown error while initializing "ParforProgressStarter2":';
                        msg{2} = me.message;
                        print_error_red(msg);
                        %           % backup solution so that we can still continue.
                       % ppm.increment = nan(1, nbr_files);
                    end
                end
                parfor k=1:nTerms
                    
                    %fprintf('Jacobian calc: k=%d/%d\n',k,nTerms)
                    [xiAdj(perm,k),flag] = bicgstab(AA(perm,perm),Q(k,perm).',1e-12,30,L,U);
                    ppm.increment(k);
                    % [xiAdj(:,k),flag] = bicgstab(AA,Q(k,:).',1e-12,40,L,U);
                    if ~isequal(flag,0)
                        error('BiCGSTAB did not converge')
                    end
                end
                try % use try / catch here, since delete(struct) will raise an error.
                    delete(ppm);
                catch me %#ok<NASGU>
                end
            end
            J = full(xiAdj.'*G);
        end
        
        function Jv = ApplyJacDiscr(obj,AA,Q,Ep,sigma,map,v)
            G = obj.calcG(Ep,sigma,map);
            Jv = Q*(AA\(G*v));
        end
        
        function Jtv = ApplyJacTrDiscr(obj,AA,Q,Ep,sigma,map,v)
            G = obj.calcG(Ep,sigma,map);
            Jtv = G.'*(AA\(Q.'*v));
        end

    end %methods
end
