classdef Tetmesh < handle
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Mesh class for 3D tetrahedral mesh
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Input: p - point matrix (np x 3)
    %        t - connectivity (nt x 4)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Properties: p - point matrix (np x 3)
    %             t - sorted connectivity (nt x 4)
    %             edges - unique sorted edges (ne,2)
    %             faces - unique sorted faces (ne,3)
    %             edgeLoc - edge numbering convention table
    %             bdFaMask - logical mask for boundary faces
    %             bdFlag - boundary flag for faces
    %                      (0=not boundary,1=Dirichlet,2=Neumann)
    %             el2ed - element to edge connectivity (nt,6)
    %             el2fa - element to face connectivity (nt,4)
    %             p2ed - node to edge incidence matrix (np x np)
    %             B,b,detB,invBT - affine map ( B*x + b )
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Methods   : calcCenter - calculate centers of elements
    %             calcLength - calculate lengths of edges
    %             calcVolume - calculate volumes of elements
    %             plotMesh - plot mesh
    %             affineTet - pre-calculate affine map information
    %             setBC - set boundary condition with bdFlag
    %                     using a logical expression
    %             getDiscreteGrad - create faces - cells incidence matrix
    %             calcTetProp - calculate properties of a tetrahedron
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Copyright Dimitris Kamilis, 2017
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % References:
    % (1) HJ-FEM: Implementing the finite element assembly in
    % interpreted languages, Antti Hannukainen and Mika Juntunen
    % Some concepts on the mesh structure implementation are based on
    % (2) iFEM: An innovative finite element method package in MATLAB
    % by Long Chen
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    properties (SetAccess = private)
        p
        t
        np
        nt
        ne
        nf
        edges
        faces
        el2ed
        el2fa
        fa2el
        fa2ed
        p2ed
        edgeLoc
        faceLoc
        bdFaMask
        bdFlag
        B
        b
        detB
        invBT
        tidx
        activeCells
        Dgrad
        fasigns
        signs
        bdFaActive
    end

    methods
        function obj = Tetmesh(p,t,varargin)
            % Constructor
            if nargin==2
                if size(p,2)==3 && size(t,2)==4
                    [t, obj.tidx] = sort(t,2);
                    obj.p = p;
                    obj.t = uint32(t);
                    obj.nt = size(obj.t,1);
                    obj.np = size(obj.p,1);
                    obj.edgeLoc = uint8([1 2; 1 3; 1 4; 2 3; 2 4; 3 4]);
                    obj.faceLoc = uint8([1 2 3;1 2 4;1 3 4;2 3 4]);
                    allEdges = uint32([obj.t(:,[1 2]); obj.t(:,[1 3]); obj.t(:,[1 4]); ...
                        obj.t(:,[2 3]); obj.t(:,[2 4]); obj.t(:,[3 4])]);
                    [obj.edges, ~, ic] = unique(allEdges,'rows');
                    obj.el2ed = uint32(reshape(ic,obj.nt,6));
                    obj.ne = size(obj.edges,1);
                    counts = accumarray(ic, ones(size(ic)), [], @sum);
                    allFaces = uint32([obj.t(:,[1 2 3]);obj.t(:,[1 2 4]); ...
                        obj.t(:,[1 3 4]);obj.t(:,[2 3 4])]);                    
                    [obj.faces, ia, j] = unique(allFaces,'rows');
                    obj.el2fa = uint32(reshape(j,obj.nt,4));
                    counts = accumarray(j, ones(size(j)), [], @sum);
                    obj.bdFaMask = counts == 1;
                    obj.nf = size(obj.faces,1);
                    [~,LocB] = ismember(1:obj.nf,obj.el2fa);
                    [~,LocBB] = ismember(1:obj.nf,obj.el2fa(end:-1:1));
                    [obj.fa2el,~] = ind2sub(size(obj.el2fa),LocB);
                    [obj.fa2el(2,:),~] = ind2sub(size(obj.el2fa), LocBB);
                    obj.fa2el(2,:) = obj.nt + 1 - obj.fa2el(2,:);
                    test1 = obj.fa2el(1,:) == obj.fa2el(2,:);
                    obj.fa2el(2,test1) = 0;
                    obj.fa2el = uint32(obj.fa2el);
                    obj.affineTet();
                    obj.activeCells = true([obj.nt,1]);                    
                else
                    error('Only tetrahedral mesh in 3D supported')
                end
            else
                error ('Input: (PointMatrix,ConnectivityMatrix) ')
            end
        end
        
        function C = getDiscreteGrad(obj)
            %#rows = #cells
            %#cols = #faces
            %nActive = sum(obj.activeCells);
            C = repmat([-1 1 -1 1], [obj.nt 1]);
            obj.signs = ones(obj.nt,1);
            obj.signs(obj.detB < 0) = -1;
            C = bsxfun(@times,C, obj.signs);
            colind = repmat(1:obj.nt,4,1);
            colind = colind(:);
            rowind = double(obj.el2fa.');
            rowind = rowind(:);
            C = permute(C,[2,3,1]);    
            C = sparse(rowind,colind,C(:),obj.nf,obj.nt); % incidence matrix faces-cells
            C = C.';
        end

        function c = calcCenter(obj)
            c = (obj.p(obj.t(:,1),:) + obj.p(obj.t(:,2),:) + ...
                obj.p(obj.t(:,3),:) + obj.p(obj.t(:,4),:))/4;
        end
        
        function findBdActive(obj)
            % find faces that have all three vertices (almost) along the
            % plane z=0 
            tol = 1;
            obj.bdFaActive = obj.bdFaMask;
            for ii=1:obj.nf
                verticesz = obj.p(obj.faces(ii,:),3);
                if all(verticesz > 0 - tol) && all(verticesz < 0 + tol)
                    obj.bdFaActive(ii) = true;
                end  
                if any(verticesz > 0 + tol)
                    obj.bdFaActive(ii) = true;
                end
            end
        end

        
        function coords = getBaryCoords(obj, points)
             identifier = num2str(rows(points));
             file = strcat('barycoords',identifier);
             file = strcat(file,'.mat');
             try
                load(file)
                assert(isequal(size(coords),[rows(points) 5]))
             catch
                [l1,l2,l3,l4,id] = obj.toBarycentric(points);
                coords = [l1,l2,l3,l4,id];
                save(file,'coords')
             end
        end

        function len = calcLength(obj)
            x = reshape(obj.p(obj.t,1),obj.nt,4);
            y = reshape(obj.p(obj.t,2),obj.nt,4);
            z = reshape(obj.p(obj.t,3),obj.nt,4);
            len = zeros(6,obj.nt);
            len(1,:) = sqrt((x(:,1)-x(:,2)).^2+(y(:,1)-y(:,2)).^2+(z(:,1)-z(:,2)).^2);
            len(2,:) = sqrt((x(:,1)-x(:,3)).^2+(y(:,1)-y(:,3)).^2+(z(:,1)-z(:,3)).^2);
            len(3,:) = sqrt((x(:,1)-x(:,4)).^2+(y(:,1)-y(:,4)).^2+(z(:,1)-z(:,4)).^2);
            len(4,:) = sqrt((x(:,2)-x(:,3)).^2+(y(:,2)-y(:,3)).^2+(z(:,2)-z(:,3)).^2);
            len(5,:) = sqrt((x(:,2)-x(:,4)).^2+(y(:,2)-y(:,4)).^2+(z(:,2)-z(:,4)).^2);
            len(6,:) = sqrt((x(:,3)-x(:,4)).^2+(y(:,3)-y(:,4)).^2+(z(:,3)-z(:,4)).^2);
            len = len';
        end

        function [inradius, incenter, outradius, outcenter, quality] = calcTetProp(obj, id)
            nid = length(id);
            TR = triangulation(reshape(1:nid*4,nid,4),obj.p(obj.t(id,:),:));
            [outcenter, outradius] = TR.circumcenter((1:nid).');
            [incenter, inradius] = TR.incenter((1:nid).');
            quality = 3*inradius./outradius;
        end
        
        function [normal,area,unitN] = facenormal(obj)
            v12 = obj.p(obj.faces(:,2),:) - obj.p(obj.faces(:,1),:);
            v13 = obj.p(obj.faces(:,3),:) - obj.p(obj.faces(:,1),:);
            normal = mycross(v12,v13,2);
            area = sqrt(sum(normal.^2,2))/2;
            unitN = 0.5*normal./repmat(area,[1,3]);
            
        end

        function m = calcMid(obj)
            m = (obj.p(obj.edges(:,2),:)-obj.p(obj.edges(:,1),:))/2 + obj.p(obj.edges(:,1),:);
        end

        function V = calcVolume(obj)
            V = obj.detB/6;
        end
        
     function V = nodeQuantityInterp(obj, nodeVal, barycoords)
            if cols(barycoords)==3
                %coords = quad.X.';
                barycoords(:,4) = ones(rows(barycoords),1)-sum(barycoords,2);
            elseif cols(barycoords)==4
                
            else
                error('Wrong number of columns')
            end
            %[~,b,c,d] = obj.calcHatGradients();
                sol1 = nodeVal(obj.t(:,1));
                sol2 = nodeVal(obj.t(:,2));
                sol3 = nodeVal(obj.t(:,3));
                sol4 = nodeVal(obj.t(:,4));
                V = barycoords(:,1)*sol1.' + barycoords(:,2)*sol2.' + barycoords(:,3)*sol3.' + barycoords(:,4)*sol4.';
        end
        
         function [Ex,Ey,Ez] = edgeQuantityInterp(obj, sol, quad)
            % Interpolate solution at quadrature points            
            % Barycentric coordinates
            coords = quad.X.';
            coords(:,4) = ones(quad.nint,1)-sum(coords,2);
            % p1 = obj.maxw.mesh.p(obj.maxw.mesh.t(:,2),:);
            % p2 = obj.maxw.mesh.p(obj.maxw.mesh.t(:,3),:);
            % p3 = obj.maxw.mesh.p(obj.maxw.mesh.t(:,4),:);
            % p4 = obj.maxw.mesh.p(obj.maxw.mesh.t(:,1),:);
            % psub = kron(p1,coords(:,1)) + kron(p2,coords(:,2)) + kron(p3,coords(:,3)) + ...
            %    kron(p4,coords(:,4));                       
            [~,b,c,d] = obj.calcHatGradients(); % Gradients          
            % Solution values            
            sol1 = sol(obj.el2ed(:,1));
            sol2 = sol(obj.el2ed(:,2));
            sol3 = sol(obj.el2ed(:,3));
            sol4 = sol(obj.el2ed(:,4));
            sol5 = sol(obj.el2ed(:,5));
            sol6 = sol(obj.el2ed(:,6));           
            % Field values (nt X ne)
            Ex = coords(:,1)*(sol1.*b(:,2) + sol2.*b(:,3) + sol3.*b(:,4)).' + ...
                coords(:,2)*(-sol1.*b(:,1) + sol4.*b(:,3) + sol5.*b(:,4)).' + ...
                coords(:,3)*(-sol2.*b(:,1) + sol6.*b(:,4) - sol4.*b(:,2)).' + ...
                coords(:,4)*(-sol3.*b(:,1) - sol5.*b(:,2) - sol6.*b(:,3)).';
            
            Ey = coords(:,1)*(sol1.*c(:,2) + sol2.*c(:,3) + sol3.*c(:,4)).' + ...
                coords(:,2)*(-sol1.*c(:,1) + sol4.*c(:,3) + sol5.*c(:,4)).' + ...
                coords(:,3)*(-sol2.*c(:,1) + sol6.*c(:,4) - sol4.*c(:,2)).' + ...
                coords(:,4)*(-sol3.*c(:,1) - sol5.*c(:,2) - sol6.*c(:,3)).';
            
            Ez = coords(:,1)*(sol1.*d(:,2) + sol2.*d(:,3) + sol3.*d(:,4)).' + ...
                coords(:,2)*(-sol1.*d(:,1) + sol4.*d(:,3) + sol5.*d(:,4)).' + ...
                coords(:,3)*(-sol2.*d(:,1) + sol6.*d(:,4) - sol4.*d(:,2)).' + ...
                coords(:,4)*(-sol3.*d(:,1) - sol5.*d(:,2) - sol6.*d(:,3)).'; 
        end

        function [l1, l2, l3, l4, id] = toBarycentric(obj, cartCoords)
         %    TR = triangulation(double(obj.t),obj.p);
         %     barc = cartesianToBarycentric(TR,[1:obj.nt]',repmat(cartCoords,obj.nt,1));
         %     u1B = barc(:,1);
         %     u2B = barc(:,2);
         %     u3B = barc(:,3);
         %     u4B = barc(:,4);
            pbx = cartCoords(1) - obj.b{1};
            pby = cartCoords(2) - obj.b{2};
            pbz = cartCoords(3) - obj.b{3};
            iBT = cat(3,obj.invBT{:});
            u1 = pbx.*iBT(:,1,1) + pby.*iBT(:,1,2) + pbz.*iBT(:,1,3);
            u2 = pbx.*iBT(:,2,1) + pby.*iBT(:,2,2) + pbz.*iBT(:,2,3);
            u3 = pbx.*iBT(:,3,1) + pby.*iBT(:,3,2) + pbz.*iBT(:,3,3);
            u4 = 1-u1-u2-u3;
            [id,~] = find(u1 > 0.0 & u2 > 0.0 & u3 > 0.0 & u4 > 0.0);
            assert(length(id)==1);
            T = [-1,-1,-1;1,0,0;0,1,0];
            lamda = T*[u1(id),u2(id),u3(id)-1]';
            l1 = lamda(1);
            l2 = lamda(2);
            l3 = lamda(3);
            l4 = 1-l1-l2-l3;
        end

        function cartCoords = toCartesian(obj, barCoords, id)            
            cartCoords = ([obj.B{1}(id,:);obj.B{2}(id,:);obj.B{3}(id,:)] * barCoords(1:3).' + [obj.b{1}(id,:); obj.b{2}(id,:); obj.b{3}(id,:)]).';
        end

        function plotMesh(obj,varargin)
            if nargin > 1
                tetramesh(obj.t,obj.p,varargin{1})
            else
                tetramesh(obj.t,obj.p)
            end
        end

        function setBC(obj, type, varargin)
            if strcmp(type,'Dirichlet')
                type = 1;
            elseif strcmp(type,'Neumann')
                type = 2;
            else
                error('Invalid boundary condition type')
            end
            if nargin > 2
                expr = varargin{1};
            else
                expr = '';
            end
            obj.bdFlag = zeros(obj.nf,1);
            x = (obj.p(obj.faces(obj.bdFaMask,1),1) + ...
                obj.p(obj.faces(obj.bdFaMask,2),1) + ...
                obj.p(obj.faces(obj.bdFaMask,3),1))/3;
            y = (obj.p(obj.faces(obj.bdFaMask,1),2) + ...
                obj.p(obj.faces(obj.bdFaMask,2),2) + ...
                obj.p(obj.faces(obj.bdFaMask,3),2))/3;
            z = (obj.p(obj.faces(obj.bdFaMask,1),3) + ...
                obj.p(obj.faces(obj.bdFaMask,2),3) + ...
                obj.p(obj.faces(obj.bdFaMask,3),3))/3;
            try
                idx = eval(expr);
                assert(islogical(idx));
            catch
                idx = true(size(obj.faces(obj.bdFaMask),1),1);
            end
            rowsid = find(obj.bdFaMask);
            obj.bdFlag(rowsid(idx)) = type;
        end

        function extraStruct(obj)
            obj.p2ed = sparse(double(obj.edges(:,[1,2])),double(obj.edges(:,[2,1])),...
                [1:obj.ne,1:obj.ne],obj.np,obj.np);
            obj.fa2ed(obj.el2fa(:,4),:) = obj.el2ed(:,[4 5 6]);
            obj.fa2ed(obj.el2fa(:,3),:) = obj.el2ed(:,[2 3 6]);
            obj.fa2ed(obj.el2fa(:,2),:) = obj.el2ed(:,[1 3 5]);
            obj.fa2ed(obj.el2fa(:,1),:) = obj.el2ed(:,[1 2 4]);
        end

        function exportMeshCellData(obj, filename, data)
            fid = fopen(filename, 'w');
            fprintf(fid, '<?xml version="1.0"?>\n');
            fprintf(fid, '<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">\n');
            fprintf(fid, '<UnstructuredGrid>\n');
            fprintf(fid, '<Piece NumberOfPoints="%u" NumberOfCells="%u">\n',obj.np,obj.nt);
            fprintf(fid, '<CellData>\n');
            fprintf(fid, '<DataArray type="Float32" Name="Data" NumberOfComponents="1" format="ascii">\n');
            fprintf(fid, '%6.12f ',[data(:)]');
            fprintf(fid, '\n</DataArray>\n');
            fprintf(fid, '</CellData>\n');
            fprintf(fid, '<Points>\n');
            fprintf(fid, '<DataArray type="Float32" Name="position" NumberOfComponents="3" format="ascii">\n');
            fprintf(fid, '%6.12f %6.12f %6.12f ',obj.p.');
            fprintf(fid, '\n</DataArray>\n');
            fprintf(fid, '</Points>\n');
            fprintf(fid, '<Cells>\n');
            fprintf(fid, '<DataArray type="Int32" Name="connectivity" NumberOfComponents="1" format="ascii">\n');
            fprintf(fid, '%u %u %u %u ',obj.t.'-1);
            fprintf(fid, '\n</DataArray>\n');
            fprintf(fid, '<DataArray type="Int32" Name="offsets" NumberOfComponents="1" format="ascii">\n');
            fprintf(fid, '%d ',4*[1:obj.nt]);
            fprintf(fid, '\n</DataArray>\n');
            fprintf(fid, '<DataArray type="UInt8" Name="types" NumberOfComponents="1" format="ascii">\n');
            a=10; % type of element=tetrahedra
            fprintf(fid, '%u ',a(ones(1,obj.nt)));
            fprintf(fid, '\n</DataArray>\n');
            fprintf(fid, '</Cells>\n');
            fprintf(fid, '</Piece>\n');
            fprintf(fid, '</UnstructuredGrid>\n');
            fprintf(fid, '</VTKFile>');
        end
        function exportMeshNodeData(obj, filename, data)
            fid = fopen(strcat(filename,'.msh.vtu'), 'w');
            fprintf(fid, '<?xml version="1.0"?>\n');
            fprintf(fid, '<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">\n');
            fprintf(fid, '<UnstructuredGrid>\n');
            fprintf(fid, '<Piece NumberOfPoints="%u" NumberOfCells="%u">\n',obj.np,obj.nt);
            fprintf(fid, '<PointData>\n');
            fprintf(fid, '<DataArray type="Float64" Name="Field" NumberOfComponents="1" format="ascii">\n');
            fprintf(fid, '%6.15e ',[data(:)]');
            fprintf(fid, '\n</DataArray>\n');
            fprintf(fid, '</PointData>\n');
            fprintf(fid, '<Points>\n');
            fprintf(fid, '<DataArray type="Float64" Name="position" NumberOfComponents="3" format="ascii">\n');
            fprintf(fid, '%6.12f %6.12f %6.12f ',obj.p.');
            fprintf(fid, '\n</DataArray>\n');
            fprintf(fid, '</Points>\n');
            fprintf(fid, '<Cells>\n');
            fprintf(fid, '<DataArray type="Int32" Name="connectivity" NumberOfComponents="1" format="ascii">\n');
            fprintf(fid, '%u %u %u %u ',obj.t.'-1);
            fprintf(fid, '\n</DataArray>\n');
            fprintf(fid, '<DataArray type="Int32" Name="offsets" NumberOfComponents="1" format="ascii">\n');
            fprintf(fid, '%d ',4*[1:obj.nt]);
            fprintf(fid, '\n</DataArray>\n');
            fprintf(fid, '<DataArray type="UInt8" Name="types" NumberOfComponents="1" format="ascii">\n');
            a=10; % type of element=tetrahedra
            fprintf(fid, '%u ',a(ones(1,obj.nt)));
            fprintf(fid, '\n</DataArray>\n');
            fprintf(fid, '</Cells>\n');
            fprintf(fid, '</Piece>\n');
            fprintf(fid, '</UnstructuredGrid>\n');
            fprintf(fid, '</VTKFile>');
        end
        
         function exportMeshQuadData(obj, filename, data, quad)            
             basis = NGrad(quad);
             fid = fopen(strcat(filename,'.msh.vtu'), 'w');
             fprintf(fid, '<?xml version="1.0"?>\n');
             fprintf(fid, '<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">\n');
             fprintf(fid, '<UnstructuredGrid>\n');
             fprintf(fid, '<FieldData>\n');
             fprintf(fid, '<DataArray type="Float32" Name="Field" NumberOfTuples="%u" format="ascii">\n', length(data));
             fprintf(fid, '%6.12f ',[data(:)].');  
             fprintf(fid, '\n<InformationKey name="QUADRATURE_OFFSET_ARRAY_NAME" location="vtkQuadratureSchemeDefinition" value="QuadratureOffset">\n');
             fprintf(fid, '</InformationKey>\n');
             fprintf(fid, '</DataArray>\n');
             %fprintf(fid, '<DataArray type="Float32" Name="ElectricFieldIm" NumberOfComponents="1" format="ascii">\n');
             %fprintf(fid, '%6.12f ',[imag(Ex(:))].');
             %fprintf(fid, '\n<InformationKey name="QUADRATURE_OFFSET_ARRAY_NAME" location="vtkQuadratureSchemeDefinition" value="QuadratureOffset">\n');
             %fprintf(fid, '</InformationKey>\n');
             %fprintf(fid, '\n</DataArray>\n');
             fprintf(fid, '</FieldData>\n');
             fprintf(fid, '<Piece NumberOfPoints="%u" NumberOfCells="%u">\n',obj.np,obj.nt);
             fprintf(fid, '<CellData>\n');
             fprintf(fid, '<DataArray type="Int32" Name="QuadratureOffset" format="ascii">\n');
             fprintf(fid, '%d ',quad.nint*[0:obj.nt-1]);
             fprintf(fid, '\n<InformationKey name="DICTIONARY" location="vtkQuadratureSchemeDefinition">\n');
             fprintf(fid, '<vtkQuadratureSchemeDefinition>\n');
             fprintf(fid, '<CellType value="10"/>\n');
             fprintf(fid, '<NumberOfNodes value="4"/>\n');
             fprintf(fid, '<NumberOfQuadraturePoints value="%u"/>',quad.nint);
             fprintf(fid, '\n<ShapeFunctionWeights>\n');
             fprintf(fid, '%u ',basis.N{1});
             fprintf(fid, '\n');
             fprintf(fid, '%u ',basis.N{2});
              fprintf(fid, '\n');
             fprintf(fid, '%u ',basis.N{3});
              fprintf(fid, '\n');
             fprintf(fid, '%u ',basis.N{4});
             fprintf(fid, '\n</ShapeFunctionWeights>\n');
             fprintf(fid, '<QuadratureWeights>\n');
             fprintf(fid, '%u ',quad.W.');
             fprintf(fid, '\n</QuadratureWeights>\n');
             fprintf(fid, '</vtkQuadratureSchemeDefinition>\n');
             fprintf(fid, '</InformationKey>\n');
             fprintf(fid, '</DataArray>\n');
             fprintf(fid, '</CellData>\n');
             fprintf(fid, '<Points>\n');
             fprintf(fid, '<DataArray type="Float32" Name="position" NumberOfComponents="3" format="ascii">\n');
             fprintf(fid, '%6.12f %6.12f %6.12f ',obj.p.');
             fprintf(fid, '\n</DataArray>\n');
             fprintf(fid, '</Points>\n');
             fprintf(fid, '<Cells>\n');
             fprintf(fid, '<DataArray type="Int32" Name="connectivity" NumberOfComponents="1" format="ascii">\n');
             fprintf(fid, '%u %u %u %u ',obj.t.'-1);
             fprintf(fid, '\n</DataArray>\n');
             fprintf(fid, '<DataArray type="Int32" Name="offsets" NumberOfComponents="1" format="ascii">\n');
             fprintf(fid, '%d ',4*[1:obj.nt]);
             fprintf(fid, '\n</DataArray>\n');
             fprintf(fid, '<DataArray type="UInt8" Name="types" NumberOfComponents="1" format="ascii">\n');
             a=10; % type of element=tetrahedra
             fprintf(fid, '%u ',a(ones(1,obj.nt)));
             fprintf(fid, '\n</DataArray>\n');
             fprintf(fid, '</Cells>\n');
             fprintf(fid, '</Piece>\n');
             fprintf(fid, '</UnstructuredGrid>\n');
             fprintf(fid, '</VTKFile>');
             fclose(fid);
         end
    
        function setActive(obj, foo)
            center = obj.calcCenter;
            obj.activeCells = foo(center);
            % find boundary for activeCells            
            obj.findBdActive();            
        end


        function Q = obsOp(obj,pos)
            % Construct interpolation operator Q for selected points
            l = size(pos,1);
            [~,b,c,d] = obj.calcHatGradients(); % Gradients
            Qx = zeros(l,obj.ne);
            Qy = Qx;
            Qz = Qx;
            for i=1:l
                [u1,u2,u3,u4,id] = obj.toBarycentric(pos(i,:));
                assert(length(id)==1)
                dofs = obj.el2ed(id,:);
                Qx(i,dofs) = [u1*b(id,2)-u2*b(id,1),u1*b(id,3)-u3*b(id,1),u1*b(id,4)-u4*b(id,1), ...
                              u2*b(id,3)-u3*b(id,2),u2*b(id,4)-u4*b(id,2),u3*b(id,4)-u4*b(id,3)];
                Qy(i,dofs) = [u1*c(id,2)-u2*c(id,1),u1*c(id,3)-u3*c(id,1),u1*c(id,4)-u4*c(id,1), ...
                              u2*c(id,3)-u3*c(id,2),u2*c(id,4)-u4*c(id,2),u3*c(id,4)-u4*c(id,3)];
                Qz(i,dofs) = [u1*d(id,2)-u2*d(id,1),u1*d(id,3)-u3*d(id,1),u1*d(id,4)-u4*d(id,1), ...
                              u2*d(id,3)-u3*d(id,2),u2*d(id,4)-u4*d(id,2),u3*d(id,4)-u4*d(id,3)];
            end
            Q = sparse([Qx;Qy;Qz]);
        end
        
        function [V,b,c,d] = calcHatGradients(obj)
            % Calculate area and gradients for hat basis functions
            % in 3D tetrahedral mesh
            % Output: Volume of tetrahedron and gradient=[b,c,d]'
            V = abs(obj.detB)/6;
            % Gradients in reference tetrahedron
            gradPhi(:,1) = [-1;-1;-1];
            gradPhi(:,2) = [1;0;0];
            gradPhi(:,3) = [0;1;0];
            gradPhi(:,4) = [0;0;1];
            b = zeros(obj.nt,4);
            c = b;
            d = b;
            % Mapping
            for i=1:4
                b(:,i) = obj.invBT{1}*gradPhi(:,i);
                c(:,i) = obj.invBT{2}*gradPhi(:,i);
                d(:,i) = obj.invBT{3}*gradPhi(:,i);
            end
        end
        
        function intx = glbIntx(obj, tetquad, resh)            
            intx = zeros(tetquad.nint,obj.nt,3);
            for i = 1:3
                intx(:,:,i) = bsxfun(@plus, obj.B{i}*tetquad.X,obj.b{i}).';
            end
            if resh %reshape to (NT*NINT) X 3
                intx = reshape(intx,obj.nt*tetquad.nint,3);
            end
        end


        function I = calcIntegral(obj, domain, quadRule, coefficient, varargin)
            nt = sum(obj.activeCells);
            l = nt*quadRule.nint;
            x = zeros(quadRule.nint,nt,3);
            % Global integration points
            for i=1:3
                x(:,:,i) = bsxfun(@plus, obj.B{i}(obj.activeCells,:)*quadRule.X, obj.b{i}(obj.activeCells)).';
            end
            if nargin==6
                if isa(varargin{1},'function_handle') && isa(varargin{2},'function_handle')
                    intp = reshape(x,l,3);
                    try
                        P = varargin{1}(intp(:,1),intp(:,2),intp(:,3));
                        assert(isequal(size(P,1),l))
                    catch err
                        P = cell2mat(arrayfun(varargin{1},intp(:,1),intp(:,2),intp(:,3),'UniformOutput',false));
                    end
                    P1x = reshape(P(:,1),quadRule.nint,nt);
                    P1y = reshape(P(:,2),quadRule.nint,nt);
                    P1z = reshape(P(:,3),quadRule.nint,nt);
                    try
                        P = varargin{2}(intp(:,1),intp(:,2),intp(:,3));
                        assert(isequal(size(P,1),l))
                    catch err
                        P = cell2mat(arrayfun(varargin{2},intp(:,1),intp(:,2),intp(:,3),'UniformOutput',false));
                    end
                    P2x = reshape(P(:,1),quadRule.nint,nt);
                    P2y = reshape(P(:,2),quadRule.nint,nt);
                    P2z = reshape(P(:,3),quadRule.nint,nt);
                else
                    error('Wrong input')
                end
            elseif nargin>7
                if nargin==8
                    if isa(varargin{4},'function_handle')
                        intp = reshape(x,l,3);
                        P = varargin{1}(intp(:,1),intp(:,2),intp(:,3));
                        P2x = reshape(P(:,1),quadRule.nint,obj.nt);
                        P2y = reshape(P(:,2),quadRule.nint,obj.nt);
                        P2z = reshape(P(:,3),quadRule.nint,obj.nt);
                        P1x = varargin{1};
                        P1y = varargin{2};
                        P1z = varargin{3};
                    else
                        error('Wrong input')
                    end
                elseif nargin==9
                    if isa(varargin{4},'function_handle') && isvector(varargin{5})
                        intp = reshape(x,l,3);
                        P = varargin{1}(intp(:,1),intp(:,2),intp(:,3));
                        P2x = reshape(P(:,1),quadRule.nint,obj.nt);
                        P2y = reshape(P(:,2),quadRule.nint,obj.nt);
                        P2z = reshape(P(:,3),quadRule.nint,obj.nt);
                        P2x = bsxfun(@times,P2x.',varargin{2}).';
                        P2y = bsxfun(@times,P2y.',varargin{2}).';
                        P2z = bsxfun(@times,P2z.',varargin{2}).';
                        P1x = varargin{1};
                        P1y = varargin{2};
                        P1z = varargin{3};
                    else
                        error('Wrong input')
                    end
                elseif nargin==10
                    P1x = varargin{1};
                    P1y = varargin{2};
                    P1z = varargin{3};
                    P2x = varargin{4};
                    P2y = varargin{5};
                    P2z = varargin{6};
                    P1x = reshape(P1x,quadRule.nint,nt);
                    P1y = reshape(P1y,quadRule.nint,nt);
                    P1z = reshape(P1z,quadRule.nint,nt);
                    P2x = reshape(P2x,quadRule.nint,nt);
                    P2y = reshape(P2y,quadRule.nint,nt);
                    P2z = reshape(P2z,quadRule.nint,nt);
                else
                    error('Wrong input')
                end
            elseif nargin==7
                P1x = varargin{1};
                P1y = varargin{2};
                P1z = varargin{3};
                P2x = P1x;
                P2y = P1y;
                P2z = P1z;
            end

            %TODO: anisotropic inhomogeneous
            if isscalar(coefficient)
                coefficient = repmat(coefficient,[1,obj.nt]);
            elseif ismatrix(coefficient)
                if ~isequal(size(coefficient),[1,obj.nt])
                    coefficient = coefficient.';
                    if ~isequal(size(coefficient),[1, obj.nt])
                        error('Incorrect coefficient format')
                    end
                end
            end

            I = sum(bsxfun(@times,(P1x.*P2x) + (P1y.*P2y) + ...
                (P1z.*P2z),quadRule.W),1);
            I = coefficient(obj.activeCells).*I.*abs(obj.detB(obj.activeCells)).';
            if strcmp(domain,'whole')
                I = sum(I);
            elseif strcmp(domain,'piecewise')
                % do nothing
            elseif islogical(domain)
                I(domain) = 0;
                I = sum(I);
            else
                error('Incorrect domain specification ("whole" for integral over all domain, "piecewise" for element-wise integral)')
            end
        end
    end
        methods (Access=private)
            function affineTet(obj)
                % The vectorized affine mapping consists out of three matrices,
                % B{1},B{2},B{3} and three vectors b{1},b{2},b{3}. Row i in B{1} corresponds
                % to affine mapping of component x from the reference element to
                % element i.
                %
                % The classical affine mapping from reference coordinates (x,y,z)
                % to element i is
                %
                % [ B{1}(i,:)  [x    [bx(i)
                %   B{2}(i,:)   y     by(i)
                %   B{3}(i,:) ] z] +  bz(i)];
                %
                % Based on HJ-FEM, Antti Hannukainen


                B{1} = [(obj.p(obj.t(:,2),1)-obj.p(obj.t(:,1),1)) ...
                    (obj.p(obj.t(:,3),1)-obj.p(obj.t(:,1),1)) ...
                    (obj.p(obj.t(:,4),1)-obj.p(obj.t(:,1),1))];
                B{2} = [(obj.p(obj.t(:,2),2)-obj.p(obj.t(:,1),2)) ...
                    (obj.p(obj.t(:,3),2)-obj.p(obj.t(:,1),2)) ...
                    (obj.p(obj.t(:,4),2)-obj.p(obj.t(:,1),2))];
                B{3} = [(obj.p(obj.t(:,2),3)-obj.p(obj.t(:,1),3)) ...
                    (obj.p(obj.t(:,3),3)-obj.p(obj.t(:,1),3)) ...
                    (obj.p(obj.t(:,4),3)-obj.p(obj.t(:,1),3))];

                obj.b{1} = obj.p(obj.t(:,1),1);
                obj.b{2} = obj.p(obj.t(:,1),2);
                obj.b{3} = obj.p(obj.t(:,1),3);

                obj.detB = -B{1}(:,3).*B{2}(:,2).*B{3}(:,1) + B{1}(:,2).*B{2}(:,3).*B{3}(:,1) ...
                    +B{1}(:,3).*B{2}(:,1).*B{3}(:,2) - B{1}(:,1).*B{2}(:,3).*B{3}(:,2) ...
                    -B{1}(:,2).*B{2}(:,1).*B{3}(:,3) + B{1}(:,1).*B{2}(:,2).*B{3}(:,3);

                obj.invBT{1} = [ (-B{2}(:,3).*B{3}(:,2)+B{2}(:,2).*B{3}(:,3))./obj.detB ...
                    ( B{2}(:,3).*B{3}(:,1)-B{2}(:,1).*B{3}(:,3))./obj.detB ...
                    (-B{2}(:,2).*B{3}(:,1)+B{2}(:,1).*B{3}(:,2))./obj.detB];

                obj.invBT{2} = [( B{1}(:,3).*B{3}(:,2)-B{1}(:,2).*B{3}(:,3))./obj.detB ...
                    (-B{1}(:,3).*B{3}(:,1)+B{1}(:,1).*B{3}(:,3))./obj.detB ...
                    ( B{1}(:,2).*B{3}(:,1)-B{1}(:,1).*B{3}(:,2))./obj.detB ];

                obj.invBT{3} = [ (-B{1}(:,3).*B{2}(:,2)+B{1}(:,2).*B{2}(:,3))./obj.detB ...
                    ( B{1}(:,3).*B{2}(:,1)-B{1}(:,1).*B{2}(:,3))./obj.detB ...
                    (-B{1}(:,2).*B{2}(:,1)+B{1}(:,1).*B{2}(:,2))./obj.detB ];
                obj.B = B;
            end
        end
end
