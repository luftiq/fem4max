function [V,T] = lanczosProd(A,iter)
% Compute Lanczos tridiagonilization of A.'*A
k = size(A,2);
v = rand(k,1);
v = v/(sqrt(v'*v));
alpha = zeros(iter,1);
beta = zeros(iter,1);
V = zeros(k,iter);
V(:,1) = v;
for j=1:iter
    if j == 1
        w = A.'*(A*V(:,j));
    else
        w =  A.'*(A*V(:,j))-beta(j)*V(:,j-1);
    end
    alpha(j) = V(:,j)'*w;
    w = w - alpha(j)*V(:,j);
    beta(j+1) = sqrt(w.'*w);
    V(:,j+1) = w/beta(j+1);
end
T = spdiags([[beta(2:iter);0] alpha [0;beta(2:iter)]],[-1 0 1],iter,iter);
end
