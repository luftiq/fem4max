function [V,T] = lanczos(A,v)
k = size(A,1);
if nargin < 2 || norm(v)==0
   v = rand(k,1);
   v = v/(sqrt(v.'*v));
end
alpha = zeros(k,1);
beta = zeros(k,1);
V = zeros(k,k);
V(:,1) = v;
for j=1:k
   if j == 1
      w = A*V(:,j);
   else
      w =  A*V(:,j)-beta(j)*V(:,j-1);
   end
   alpha(j) = V(:,j)'*w;
   w = w - alpha(j)*V(:,j);
   beta(j+1) = sqrt(w.'*w);
   V(:,j+1) = w/beta(j+1);
end
T = gallery('tridiag',beta(2:k),alpha,beta(2:k));
T1 = spdiags([beta(2:k) alpha beta(2:k)],[-1 0 1]);
end