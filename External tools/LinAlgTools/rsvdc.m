function [U,S,V] = rsvdc(A,k)
%%%%%%%%%%%%%%%%%%%%%
% random SVD
%%%%%%%%%%%%%%%%%%%%%

%   Copyright 2017 Dimitris Kamilis
[M,N] = size(A);
P = min(2*k,N);
X = sqrt(1/2)*(randn(N,P)+1j*randn(N,P));
Y = A*X;
W1 = orth(Y);
B = W1'*A;
[W2,S,V] = svd(B,'econ');
U = W1*W2;
k=min(k,size(U,2));
U = U(:,1:k);
S = S(1:k,1:k);
V=V(:,1:k);
