function [V,T] = lanczos(A,varargin)
if nargin<=2
    if isa(A,'function_handle')
        error('Wrong input')
    end
    k = size(A,1);
    iter = varargin{1};
else
    
    k = varargin{1};
    iter = varargin{2};
end
v = rand(k,1);
v = v/(sqrt(v.'*v));
alpha = zeros(iter,1);
beta = zeros(iter,1);
V = zeros(k,iter);
V(:,1) = v;
for j=1:iter
    if j == 1
        if isa(A,'function_handle')
            w = A(V(:,j));
        else
            w = A*V(:,j);
        end
    else
        if isa(A,'function_handle')
            w = A(V(:,j)) - beta(j)*V(:,j-1);
        else
            w =  A*V(:,j)-beta(j)*V(:,j-1);
        end
    end
    alpha(j) = V(:,j)'*w;
    w = w - alpha(j)*V(:,j);
    beta(j+1) = sqrt(w.'*w);
    V(:,j+1) = w/beta(j+1);
end
T = spdiags([[beta(2:iter);0] alpha [0;beta(2:iter)]],[-1 0 1],iter,iter);
end