function C = diag_product(A,B )
%DIAG_PRODUCT Return the same as diag(A*B) but without calculating the
%whole of A*B
%
C = sum(A .* B', 2);
end 