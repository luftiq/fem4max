function [Uk,Sk,Vk] = rsvd2(G,k)
%-------------------------------------------------------------------------------------
% random SVD
% Extremely fast computation of the truncated Singular Value Decomposition, using
% randomized algorithms as described in Halko et al. 'finding structure with randomness
%
% usage : 
%
%  input:
%  * A(m x n) : matrix whose SVD we want with m<n
%  * K : number of components to keep
%
%  output:
%  * U,S,V : classical output as the builtin svd matlab function
%-------------------------------------------------------------------------------------

%   Copyright 2017 Dimitris Kamilis
assert(issymmetric(G));
[M,~] = size(G); %M = 2M in paper
%P = min(2*k,M); % oversampling 
X = randn(M,k); % sample a standard Gaussian matrix X (2M x k)
Y = X*G; 
Qk = orth(Y); % (2M x k)
B = Qk.'*G;
[~,Sk4,W] = svd(B*B.'); %(k x k)
Uk = Qk*W; % (2M x k)
Sk = (Sk4)^(1/4); % (k x k)
Vk = applyA(Uk/inv(Sk)); % (N x k)
