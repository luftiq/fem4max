function index = argmax(x)
%ARGMAX   Index of maximum element.
% ARGMAX(X) returns an index I such that X(I) == MAX(X(:)).
%
% See also MAX, ARGMIN.

%   Copyright 2017 Dimitris Kamilis
[ignore,index] = max(x(:));
