function x = solve_tril(T,b)
% SOLVE_TRIL      Left division by lower triangular matrix.
% SOLVE_TRIL(T,b) is the same as T\b but requires T to be lower triangular 
% and runs faster.

%   Copyright 2017 Dimitris Kamilis
error('You must run install_lightspeed in order to use this function.')
