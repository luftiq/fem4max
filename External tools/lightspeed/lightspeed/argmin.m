function index = argmin(x)
%ARGMIN   Index of minimum element.
% ARGMIN(X) returns an index I such that X(I) == MIN(X(:)).
%
% See also MIN, ARGMAX.

%   Copyright 2017 Dimitris Kamilis
[ignore,index] = min(x(:));
