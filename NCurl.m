classdef NCurl < Element
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Class for Nedelec lowest order edge elements
    % The basis functions and their curl are defined on the
    % reference tetrahedron
    % Properties: N{i} where i=1,...,6 contains the Nedelec lowest order
    %             basis functions for the reference tetrahedron evaluated
    %             at the quadrature points defined by the input object
    %             'tetquad'
    %             curlN{i} where i=1,...,6 contains the corresponding curl
    %             tetquad - an instance of the quadrature class for 3D 
    %             tetrahedra. Contains the integration points and weights
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Copyright Dimitris Kamilis, 2017
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    properties (SetAccess = private)
        N 
        curlN
        tetquad 
        order = 1;
        ndof = 6; % number of degrees of freedom
        type = 'NCurl';
    end
    
    methods
        function obj = NCurl(tetquad)
            if tetquad.dim~=3 
                error('Quadrature rule for tetraheda needed')
            end
            obj.tetquad = tetquad;
            X = tetquad.X;
            obj.N = cell(obj.ndof,1);
            obj.curlN = cell(obj.ndof,1);
            obj.N{1} = [1-X(2,:)-X(3,:) ; X(1,:) ; X(1,:)];
            obj.N{2} = [X(2,:) ; 1-X(1,:)-X(3,:) ; X(2,:)];
            obj.N{3} = [X(3,:) ; X(3,:) ; 1-X(1,:)-X(2,:)];
            obj.N{4} = [-X(2,:) ; X(1,:) ; zeros(1,tetquad.nint)];
            obj.N{5} = [-X(3,:) ; zeros(1,tetquad.nint) ; X(1,:)];
            obj.N{6} = [zeros(1,tetquad.nint) ; -X(3,:)  ; X(2,:)];
            obj.curlN{1} = repmat([0 -2 2]',1,tetquad.nint);
            obj.curlN{2} = repmat([2 0 -2]',1,tetquad.nint);
            obj.curlN{3} = repmat([-2 2 0]',1,tetquad.nint);
            obj.curlN{4} = repmat([0 0 2]',1,tetquad.nint);
            obj.curlN{5} = repmat([0 -2 0]',1,tetquad.nint);
            obj.curlN{6} = repmat([2 0 0]',1,tetquad.nint);
        end         
    end   
end
